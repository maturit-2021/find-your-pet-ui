#!/bin/bash

rm -r build &&
flutter build web --release --no-sound-null-safety && 
timestamp=$( date +"%s%3N" ) &&
cd build/web &&
sed -i "s/main.dart.js/main.dart.$timestamp.js/g" index.html &&
mv main.dart.js "main.dart.$timestamp.js"