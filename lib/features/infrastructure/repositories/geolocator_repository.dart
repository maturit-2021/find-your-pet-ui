import 'package:geolocator/geolocator.dart';

abstract class GeoLocatorRepository {
  /// Return the device latest known location
  Future<Position> getLocation();
}
