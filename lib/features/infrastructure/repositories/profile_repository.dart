import 'package:find_your_pet/features/infrastructure/model/user_profile.dart';

abstract class ProfileRepository {
  /// Retrieve an instance of the current logger user profile
  /// if there's already a provider state with a UserProfile instance
  /// ando [forceFetch] is equal to false, it will return that same state
  Future<UserProfile> me();

  Future<void> update({required UserProfile user});

  Future<void> updatePicture({
    required List<int> file,
    required String fileName,
    required UserProfile user,
  });
}
