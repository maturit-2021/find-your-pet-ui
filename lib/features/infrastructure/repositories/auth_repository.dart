import 'package:find_your_pet/features/infrastructure/model/user_profile.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/auth/login/login_state.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/auth/register/register_state.dart';

abstract class AuthRepository {
  /// Attempt to authenticated a user with the given params
  Future<UserProfile> authenticate(LoginParams params);

  /// Attempt register a new user
  Future<UserProfile> register(RegisterProfileParams params);

  /// Check if there is a valid JWT stored locally
  /// if the token is expired, the it will try to fetch a new token from the api
  /// using the refresh token
  Future<bool> checkAuthentication();

  Future<void> logout();
}
