import 'package:find_your_pet/features/data/model/track_pet_event.dart';
import 'package:rxdart/rxdart.dart';

abstract class SocketIOService {
  ValueStream<TrackPetEvent> get trackPetStream;
}
