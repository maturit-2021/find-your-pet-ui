import 'dart:convert';
import 'dart:typed_data';

class UserProfile {
  String id;
  String name;
  String city;
  Uint8List? image;

  UserProfile({
    required this.id,
    required this.name,
    required this.city,
    String? image,
  }) : this.image = image != null ? base64Decode(image) : null;

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'city': city,
    };
  }

  factory UserProfile.fromJson(Map<String, dynamic> map) {
    return UserProfile(
      id: map['id'],
      name: map['name'],
      city: map['city'],
      image: map['image'],
    );
  }

  String toJson() => json.encode(toMap());

  UserProfile copyWith({
    String? id,
    String? name,
    String? city,
    String? image,
  }) {
    return UserProfile(
      id: id ?? this.id,
      name: name ?? this.name,
      city: city ?? this.city,
      image: image ??
          (this.image != null ? base64.encode(this.image!.toList()) : null),
    );
  }

  UserProfile copyWithImage(List<int> imageBytes) {
    return this.copyWith(
      image: base64.encode(imageBytes),
    );
  }
}
