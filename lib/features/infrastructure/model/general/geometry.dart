import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:latlong2/latlong.dart';

class Geometry {
  final String type = "Point";
  final List<double> coordinates;
  Geometry({
    required this.coordinates,
  });

  Map<String, dynamic> toMap() {
    return {
      'coordinates': coordinates,
    };
  }

  factory Geometry.fromMap(Map<String, dynamic> map) {
    return Geometry(
      coordinates: List<double>.from(map['coordinates']),
    );
  }

  String toJson() => json.encode(toMap());

  factory Geometry.fromJson(String source) =>
      Geometry.fromMap(json.decode(source));

  @override
  String toString() => 'Geometry(coordinates: $coordinates)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Geometry && listEquals(other.coordinates, coordinates);
  }

  @override
  int get hashCode => coordinates.hashCode;

  double get latitude => coordinates[0];
  double get longitude => coordinates[1];

  LatLng get latLong => LatLng(coordinates[0], coordinates[1]);
}
