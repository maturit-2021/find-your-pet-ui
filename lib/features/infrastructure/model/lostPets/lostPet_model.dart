import 'package:freezed_annotation/freezed_annotation.dart';

part 'lostPet_model.freezed.dart';
part 'lostPet_model.g.dart';

enum LostPetStatus { not_found, found }

@freezed
class LostPetModel with _$LostPetModel {
  const factory LostPetModel({
    required String id,
    required DateTime createdDate,
    required LostPetStatus status,
  }) = _LostPetModel;

  factory LostPetModel.fromJson(Map<String, dynamic> json) =>
      _$LostPetModelFromJson(json);
}
