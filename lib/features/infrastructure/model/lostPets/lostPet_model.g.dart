// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lostPet_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_LostPetModel _$_$_LostPetModelFromJson(Map<String, dynamic> json) {
  return _$_LostPetModel(
    id: json['id'] as String,
    createdDate: DateTime.parse(json['createdDate'] as String),
    status: _$enumDecode(_$LostPetStatusEnumMap, json['status']),
  );
}

Map<String, dynamic> _$_$_LostPetModelToJson(_$_LostPetModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createdDate': instance.createdDate.toIso8601String(),
      'status': _$LostPetStatusEnumMap[instance.status],
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$LostPetStatusEnumMap = {
  LostPetStatus.not_found: 'not_found',
  LostPetStatus.found: 'found',
};
