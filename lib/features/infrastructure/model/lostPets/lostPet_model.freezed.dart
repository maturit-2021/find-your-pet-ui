// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'lostPet_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

LostPetModel _$LostPetModelFromJson(Map<String, dynamic> json) {
  return _LostPetModel.fromJson(json);
}

/// @nodoc
class _$LostPetModelTearOff {
  const _$LostPetModelTearOff();

  _LostPetModel call(
      {required String id,
      required DateTime createdDate,
      required LostPetStatus status}) {
    return _LostPetModel(
      id: id,
      createdDate: createdDate,
      status: status,
    );
  }

  LostPetModel fromJson(Map<String, Object> json) {
    return LostPetModel.fromJson(json);
  }
}

/// @nodoc
const $LostPetModel = _$LostPetModelTearOff();

/// @nodoc
mixin _$LostPetModel {
  String get id => throw _privateConstructorUsedError;
  DateTime get createdDate => throw _privateConstructorUsedError;
  LostPetStatus get status => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LostPetModelCopyWith<LostPetModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LostPetModelCopyWith<$Res> {
  factory $LostPetModelCopyWith(
          LostPetModel value, $Res Function(LostPetModel) then) =
      _$LostPetModelCopyWithImpl<$Res>;
  $Res call({String id, DateTime createdDate, LostPetStatus status});
}

/// @nodoc
class _$LostPetModelCopyWithImpl<$Res> implements $LostPetModelCopyWith<$Res> {
  _$LostPetModelCopyWithImpl(this._value, this._then);

  final LostPetModel _value;
  // ignore: unused_field
  final $Res Function(LostPetModel) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? createdDate = freezed,
    Object? status = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      createdDate: createdDate == freezed
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as LostPetStatus,
    ));
  }
}

/// @nodoc
abstract class _$LostPetModelCopyWith<$Res>
    implements $LostPetModelCopyWith<$Res> {
  factory _$LostPetModelCopyWith(
          _LostPetModel value, $Res Function(_LostPetModel) then) =
      __$LostPetModelCopyWithImpl<$Res>;
  @override
  $Res call({String id, DateTime createdDate, LostPetStatus status});
}

/// @nodoc
class __$LostPetModelCopyWithImpl<$Res> extends _$LostPetModelCopyWithImpl<$Res>
    implements _$LostPetModelCopyWith<$Res> {
  __$LostPetModelCopyWithImpl(
      _LostPetModel _value, $Res Function(_LostPetModel) _then)
      : super(_value, (v) => _then(v as _LostPetModel));

  @override
  _LostPetModel get _value => super._value as _LostPetModel;

  @override
  $Res call({
    Object? id = freezed,
    Object? createdDate = freezed,
    Object? status = freezed,
  }) {
    return _then(_LostPetModel(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      createdDate: createdDate == freezed
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as LostPetStatus,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_LostPetModel implements _LostPetModel {
  const _$_LostPetModel(
      {required this.id, required this.createdDate, required this.status});

  factory _$_LostPetModel.fromJson(Map<String, dynamic> json) =>
      _$_$_LostPetModelFromJson(json);

  @override
  final String id;
  @override
  final DateTime createdDate;
  @override
  final LostPetStatus status;

  @override
  String toString() {
    return 'LostPetModel(id: $id, createdDate: $createdDate, status: $status)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LostPetModel &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.createdDate, createdDate) ||
                const DeepCollectionEquality()
                    .equals(other.createdDate, createdDate)) &&
            (identical(other.status, status) ||
                const DeepCollectionEquality().equals(other.status, status)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(createdDate) ^
      const DeepCollectionEquality().hash(status);

  @JsonKey(ignore: true)
  @override
  _$LostPetModelCopyWith<_LostPetModel> get copyWith =>
      __$LostPetModelCopyWithImpl<_LostPetModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_LostPetModelToJson(this);
  }
}

abstract class _LostPetModel implements LostPetModel {
  const factory _LostPetModel(
      {required String id,
      required DateTime createdDate,
      required LostPetStatus status}) = _$_LostPetModel;

  factory _LostPetModel.fromJson(Map<String, dynamic> json) =
      _$_LostPetModel.fromJson;

  @override
  String get id => throw _privateConstructorUsedError;
  @override
  DateTime get createdDate => throw _privateConstructorUsedError;
  @override
  LostPetStatus get status => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$LostPetModelCopyWith<_LostPetModel> get copyWith =>
      throw _privateConstructorUsedError;
}
