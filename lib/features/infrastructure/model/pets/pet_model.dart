import 'dart:convert' show base64Decode;
import 'dart:typed_data';

import 'package:equatable/equatable.dart';
import 'package:find_your_pet/features/infrastructure/model/general/geometry.dart';
import 'package:find_your_pet/features/infrastructure/model/lostPets/lostPet_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jiffy/jiffy.dart';

part 'pet_model.freezed.dart';
part 'pet_model.g.dart';

@freezed
class PetModel extends Equatable with _$PetModel {
  const PetModel._();

  factory PetModel({
    required String id,
    required String name,
    required DateTime createdDate,
    required String profileId,
    String? image,
    LostPetModel? lostPet,
    Geometry? lastLocation,
  }) = _PetModel;

  factory PetModel.fromJson(Map<String, dynamic> json) =>
      _$PetModelFromJson(json);

  List<Object> get props => [id, profileId];

  Uint8List? get petImage {
    if (image == null) return null;
    return base64Decode(image!);
  }

  String? get lastSeen {
    if (lostPet == null) return null;
    final date = Jiffy(lostPet!.createdDate);

    return date.startOf(Units.HOUR).fromNow();
  }
}
