// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pet_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PetModel _$_$_PetModelFromJson(Map<String, dynamic> json) {
  return _$_PetModel(
    id: json['id'] as String,
    name: json['name'] as String,
    createdDate: DateTime.parse(json['createdDate'] as String),
    profileId: json['profileId'] as String,
    image: json['image'] as String?,
    lostPet: json['lostPet'] == null
        ? null
        : LostPetModel.fromJson(json['lostPet'] as Map<String, dynamic>),
    lastLocation: json['lastLocation'] == null
        ? null
        : Geometry.fromJson(json['lastLocation'] as String),
  );
}

Map<String, dynamic> _$_$_PetModelToJson(_$_PetModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'createdDate': instance.createdDate.toIso8601String(),
      'profileId': instance.profileId,
      'image': instance.image,
      'lostPet': instance.lostPet,
      'lastLocation': instance.lastLocation,
    };
