// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'pet_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PetModel _$PetModelFromJson(Map<String, dynamic> json) {
  return _PetModel.fromJson(json);
}

/// @nodoc
class _$PetModelTearOff {
  const _$PetModelTearOff();

  _PetModel call(
      {required String id,
      required String name,
      required DateTime createdDate,
      required String profileId,
      String? image,
      LostPetModel? lostPet,
      Geometry? lastLocation}) {
    return _PetModel(
      id: id,
      name: name,
      createdDate: createdDate,
      profileId: profileId,
      image: image,
      lostPet: lostPet,
      lastLocation: lastLocation,
    );
  }

  PetModel fromJson(Map<String, Object> json) {
    return PetModel.fromJson(json);
  }
}

/// @nodoc
const $PetModel = _$PetModelTearOff();

/// @nodoc
mixin _$PetModel {
  String get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  DateTime get createdDate => throw _privateConstructorUsedError;
  String get profileId => throw _privateConstructorUsedError;
  String? get image => throw _privateConstructorUsedError;
  LostPetModel? get lostPet => throw _privateConstructorUsedError;
  Geometry? get lastLocation => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PetModelCopyWith<PetModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PetModelCopyWith<$Res> {
  factory $PetModelCopyWith(PetModel value, $Res Function(PetModel) then) =
      _$PetModelCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String name,
      DateTime createdDate,
      String profileId,
      String? image,
      LostPetModel? lostPet,
      Geometry? lastLocation});

  $LostPetModelCopyWith<$Res>? get lostPet;
}

/// @nodoc
class _$PetModelCopyWithImpl<$Res> implements $PetModelCopyWith<$Res> {
  _$PetModelCopyWithImpl(this._value, this._then);

  final PetModel _value;
  // ignore: unused_field
  final $Res Function(PetModel) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? createdDate = freezed,
    Object? profileId = freezed,
    Object? image = freezed,
    Object? lostPet = freezed,
    Object? lastLocation = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      createdDate: createdDate == freezed
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      profileId: profileId == freezed
          ? _value.profileId
          : profileId // ignore: cast_nullable_to_non_nullable
              as String,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
      lostPet: lostPet == freezed
          ? _value.lostPet
          : lostPet // ignore: cast_nullable_to_non_nullable
              as LostPetModel?,
      lastLocation: lastLocation == freezed
          ? _value.lastLocation
          : lastLocation // ignore: cast_nullable_to_non_nullable
              as Geometry?,
    ));
  }

  @override
  $LostPetModelCopyWith<$Res>? get lostPet {
    if (_value.lostPet == null) {
      return null;
    }

    return $LostPetModelCopyWith<$Res>(_value.lostPet!, (value) {
      return _then(_value.copyWith(lostPet: value));
    });
  }
}

/// @nodoc
abstract class _$PetModelCopyWith<$Res> implements $PetModelCopyWith<$Res> {
  factory _$PetModelCopyWith(_PetModel value, $Res Function(_PetModel) then) =
      __$PetModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String name,
      DateTime createdDate,
      String profileId,
      String? image,
      LostPetModel? lostPet,
      Geometry? lastLocation});

  @override
  $LostPetModelCopyWith<$Res>? get lostPet;
}

/// @nodoc
class __$PetModelCopyWithImpl<$Res> extends _$PetModelCopyWithImpl<$Res>
    implements _$PetModelCopyWith<$Res> {
  __$PetModelCopyWithImpl(_PetModel _value, $Res Function(_PetModel) _then)
      : super(_value, (v) => _then(v as _PetModel));

  @override
  _PetModel get _value => super._value as _PetModel;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? createdDate = freezed,
    Object? profileId = freezed,
    Object? image = freezed,
    Object? lostPet = freezed,
    Object? lastLocation = freezed,
  }) {
    return _then(_PetModel(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      createdDate: createdDate == freezed
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      profileId: profileId == freezed
          ? _value.profileId
          : profileId // ignore: cast_nullable_to_non_nullable
              as String,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
      lostPet: lostPet == freezed
          ? _value.lostPet
          : lostPet // ignore: cast_nullable_to_non_nullable
              as LostPetModel?,
      lastLocation: lastLocation == freezed
          ? _value.lastLocation
          : lastLocation // ignore: cast_nullable_to_non_nullable
              as Geometry?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PetModel extends _PetModel {
  _$_PetModel(
      {required this.id,
      required this.name,
      required this.createdDate,
      required this.profileId,
      this.image,
      this.lostPet,
      this.lastLocation})
      : super._();

  factory _$_PetModel.fromJson(Map<String, dynamic> json) =>
      _$_$_PetModelFromJson(json);

  @override
  final String id;
  @override
  final String name;
  @override
  final DateTime createdDate;
  @override
  final String profileId;
  @override
  final String? image;
  @override
  final LostPetModel? lostPet;
  @override
  final Geometry? lastLocation;

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PetModel &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.createdDate, createdDate) ||
                const DeepCollectionEquality()
                    .equals(other.createdDate, createdDate)) &&
            (identical(other.profileId, profileId) ||
                const DeepCollectionEquality()
                    .equals(other.profileId, profileId)) &&
            (identical(other.image, image) ||
                const DeepCollectionEquality().equals(other.image, image)) &&
            (identical(other.lostPet, lostPet) ||
                const DeepCollectionEquality()
                    .equals(other.lostPet, lostPet)) &&
            (identical(other.lastLocation, lastLocation) ||
                const DeepCollectionEquality()
                    .equals(other.lastLocation, lastLocation)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(createdDate) ^
      const DeepCollectionEquality().hash(profileId) ^
      const DeepCollectionEquality().hash(image) ^
      const DeepCollectionEquality().hash(lostPet) ^
      const DeepCollectionEquality().hash(lastLocation);

  @JsonKey(ignore: true)
  @override
  _$PetModelCopyWith<_PetModel> get copyWith =>
      __$PetModelCopyWithImpl<_PetModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_PetModelToJson(this);
  }
}

abstract class _PetModel extends PetModel {
  factory _PetModel(
      {required String id,
      required String name,
      required DateTime createdDate,
      required String profileId,
      String? image,
      LostPetModel? lostPet,
      Geometry? lastLocation}) = _$_PetModel;
  _PetModel._() : super._();

  factory _PetModel.fromJson(Map<String, dynamic> json) = _$_PetModel.fromJson;

  @override
  String get id => throw _privateConstructorUsedError;
  @override
  String get name => throw _privateConstructorUsedError;
  @override
  DateTime get createdDate => throw _privateConstructorUsedError;
  @override
  String get profileId => throw _privateConstructorUsedError;
  @override
  String? get image => throw _privateConstructorUsedError;
  @override
  LostPetModel? get lostPet => throw _privateConstructorUsedError;
  @override
  Geometry? get lastLocation => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$PetModelCopyWith<_PetModel> get copyWith =>
      throw _privateConstructorUsedError;
}
