// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterProfileParams _$RegisterProfileParamsFromJson(
    Map<String, dynamic> json) {
  return RegisterProfileParams(
    city: json['city'] as String,
    name: json['name'] as String,
    email: json['email'] as String,
    password: json['password'] as String,
  );
}

Map<String, dynamic> _$RegisterProfileParamsToJson(
        RegisterProfileParams instance) =>
    <String, dynamic>{
      'email': instance.email,
      'password': instance.password,
      'city': instance.city,
      'name': instance.name,
    };
