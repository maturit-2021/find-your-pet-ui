import 'package:equatable/equatable.dart';
import 'package:find_your_pet/features/infrastructure/model/user_profile.dart';
import 'package:json_annotation/json_annotation.dart';

part 'register_state.g.dart';

abstract class RegisterState extends Equatable {
  RegisterState();
}

class RegisterInitial extends RegisterState {
  RegisterInitial();

  @override
  List<Object> get props => [];
}

class RegisterLoading extends RegisterState {
  RegisterLoading();

  @override
  List<Object> get props => [];
}

class RegisterLoaded extends RegisterState {
  final UserProfile register;
  RegisterLoaded(this.register);

  @override
  List<Object> get props => [register];
}

class RegisterError extends RegisterState {
  final String message;
  RegisterError(this.message);

  @override
  List<Object> get props => [message];
}

@JsonSerializable()
class RegisterProfileParams extends Equatable {
  final String email, password, city, name;

  const RegisterProfileParams({
    required this.city,
    required this.name,
    required this.email,
    required this.password,
  });

  @override
  List<Object> get props => [email, password, city, name];

  Map<String, dynamic> toJson() => _$RegisterProfileParamsToJson(this);
}
