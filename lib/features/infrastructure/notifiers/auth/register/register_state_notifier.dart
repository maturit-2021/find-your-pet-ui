import 'package:find_your_pet/core/failures/failures.dart';
import 'package:find_your_pet/features/data/auth_repository.impl.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/auth/register/register_state.dart';
import 'package:find_your_pet/features/infrastructure/repositories/auth_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final registerStateNotifier =
    StateNotifierProvider<RegisterStateNotifier, RegisterState>(
  (ref) => RegisterStateNotifier(ref),
);

class RegisterStateNotifier extends StateNotifier<RegisterState> {
  final AuthRepository _authRepository;

  RegisterStateNotifier(ProviderReference ref)
      : _authRepository = ref.read(authProvider),
        super(RegisterInitial());

  void register(RegisterProfileParams params) async {
    state = RegisterLoading();

    try {
      final profile = await _authRepository.register(params);
      state = RegisterLoaded(profile);
    } on Failure catch (e) {
      state = RegisterError(e.message ?? "Something went wrong...");
    } catch (_) {
      state = RegisterError("Unkown error");
    }
  }
}
