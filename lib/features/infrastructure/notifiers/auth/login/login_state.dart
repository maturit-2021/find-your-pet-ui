import 'package:equatable/equatable.dart';
import 'package:find_your_pet/features/infrastructure/model/user_profile.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_state.g.dart';

abstract class LoginState extends Equatable {
  LoginState();
}

class LoginInitial extends LoginState {
  LoginInitial();

  @override
  List<Object> get props => [];
}

class LoginLoading extends LoginState {
  LoginLoading();

  @override
  List<Object> get props => [];
}

class LoginLoaded extends LoginState {
  final UserProfile login;
  LoginLoaded(this.login);

  @override
  List<Object> get props => [login];
}

class LoginError extends LoginState {
  final String message;
  LoginError(this.message);

  @override
  List<Object> get props => [message];
}

@JsonSerializable()
class LoginParams extends Equatable {
  final String email, password;

  const LoginParams({
    required this.email,
    required this.password,
  });

  @override
  List<Object> get props => [email, password];

  Map<String, dynamic> toJson() => _$LoginParamsToJson(this);
}
