import 'package:find_your_pet/core/failures/failures.dart';
import 'package:find_your_pet/features/data/auth_repository.impl.dart';
import 'package:find_your_pet/features/infrastructure/repositories/auth_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'login_state.dart';

final loginStateNotifier = StateNotifierProvider<AuthStateNotifier, LoginState>(
  (ref) => AuthStateNotifier(ref),
);

final checkAuthProvider = FutureProvider.autoDispose(
  (ref) => ref.read<AuthRepository>(authProvider).checkAuthentication(),
);

class AuthStateNotifier extends StateNotifier<LoginState> {
  final AuthRepository _authRepository;

  AuthStateNotifier(ProviderReference ref)
      : _authRepository = ref.read(authProvider),
        super(LoginInitial());

  void authenticate(LoginParams params) async {
    state = LoginLoading();

    try {
      final profile = await _authRepository.authenticate(params);
      state = LoginLoaded(profile);
    } on Failure catch (e) {
      state = LoginError(e.message ?? "Unkown error");
    } catch (_) {
      state = LoginError("Unkown error");
    }
  }

  Future<void> logout() async {
    try {
      await _authRepository.logout();
    } catch (_) {}
  }
}
