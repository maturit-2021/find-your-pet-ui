import 'package:find_your_pet/features/data/geolocator.impl.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final getLocationFutureProvider = FutureProvider.autoDispose((ref) async {
  final position = await ref.read(geolocatorProvider).getLocation();
  return position;
});
