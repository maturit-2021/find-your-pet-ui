import 'dart:convert';

import 'package:file_picker/file_picker.dart';
import 'package:find_your_pet/core/failures/failures.dart';
import 'package:find_your_pet/features/data/logger_repository.dart';
import 'package:find_your_pet/features/data/pet_repository.impl.dart';
import 'package:find_your_pet/features/infrastructure/model/pets/pet_model.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/pet/pet_list_state.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logger/logger.dart';

import 'pet_repository.dart';

class PetListNotifier extends StateNotifier<PetListState> {
  PetRepository _petRepository;
  Logger _logger;

  PetListNotifier({
    required ProviderReference ref,
  })  : _petRepository = ref.read(petsProvider),
        _logger = ref.read(loggerProvider),
        super(PetListInitial([]));

  /// Load and return an initial state with a list of PETs
  Future<List<PetModel>> loadList() async {
    state = PetListLoading(state.pets);
    final list = await _petRepository.myPets();
    state = PetListLoaded(List.from(list));

    return list;
  }

  /// Open a dialog to allow the user to pick an image
  /// and then send it to the server associated with the
  /// given pet
  Future<void> pickImage(PetModel pet) async {
    final index = state.pets.indexOf(pet);

    try {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowMultiple: false,
        withData: true,
        allowedExtensions: ['jpg', 'png'],
      );

      if (result == null) return null;
      state = PetListLoading(state.pets);

      final file = result.files.single;
      List<int> imageBytes = file.bytes!;
      PetModel newPet = pet.copyWith(
        image: base64Encode(imageBytes),
      );

      await _petRepository.updatePicture(
        file: imageBytes,
        fileName: file.name!,
        pet: newPet,
      );
      state.pets[index] = newPet;
      state = PetListLoaded(state.pets);
    } on Failure catch (e) {
      state = PetListError(e.message ?? 'Something went wrong', state.pets);
    } catch (e) {
      state = PetListError("Something went wrong :/", state.pets);
    }
  }

  /// Remove a pet from the user list
  Future<void> deletePet(PetModel pet) async {
    try {
      state = PetListLoading(state.pets);
      await _petRepository.deletePet(id: pet.id);
      await Future.delayed(const Duration(milliseconds: 300));
      state = PetListLoaded(state.pets..removeWhere((e) => e == pet));
    } on Failure catch (e) {
      state = PetListError(e.message ?? 'Something went wrong', state.pets);
    } catch (e) {
      state = PetListError("Something went wrong :/", state.pets);
    }
  }

  /// Add a new PET to the users list
  Future<void> addPet(String name) async {
    try {
      state = PetListLoading(state.pets);

      final pet = await _petRepository.addPet(name: name);
      List<PetModel> pets = List.from(state.pets)..add(pet);
      pets.sort((a, b) => a.name.toLowerCase().compareTo(b.name.toLowerCase()));
      state = PetListLoaded(pets);
    } on Failure catch (e) {
      _logger.e(e);
      state = PetListError(e.message ?? 'Something went wrong', state.pets);
    } catch (e) {
      _logger.e(e);
      state = PetListError("Something went wrong :/", state.pets);
    }
  }
}
