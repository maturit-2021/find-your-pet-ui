import 'package:equatable/equatable.dart';
import 'package:find_your_pet/features/infrastructure/model/pets/pet_model.dart';

abstract class PetListState extends Equatable {
  final List<PetModel> pets;
  PetListState(this.pets);
  List<Object> get props => [];
}

class PetListInitial extends PetListState {
  PetListInitial(List<PetModel> pets) : super(pets);
}

class PetListLoading extends PetListState {
  PetListLoading(List<PetModel> pets) : super(pets);
}

class PetListLoaded extends PetListState {
  PetListLoaded(List<PetModel> pets) : super(pets);
}

class PetListError extends PetListState {
  final String message;
  PetListError(this.message, List<PetModel> pets) : super(pets);
}
