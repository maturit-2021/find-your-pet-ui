import 'package:find_your_pet/features/infrastructure/model/pets/pet_model.dart';

abstract class PetRepository {
  /// Retrieve the list of pets for the current
  /// authenticated user
  Future<List<PetModel>> myPets([bool forceFetch = false]);

  /// Add a new PET to the user list
  Future<PetModel> addPet({required String name});

  Future<void> deletePet({required String id});

  Future<void> updatePicture({
    required List<int> file,
    required String fileName,
    required PetModel pet,
  });

  void resetState();
}
