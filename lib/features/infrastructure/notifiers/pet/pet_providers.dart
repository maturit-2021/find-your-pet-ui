import 'package:find_your_pet/features/data/socket_io.dart';
import 'package:find_your_pet/features/infrastructure/model/pets/pet_model.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/pet/pet_list_notifier.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

/// Listen to the track socket with the given pet
final petTrackStream = StreamProvider.autoDispose.family<PetModel, PetModel>(
  (ref, pet) {
    final stream = ref.watch(socketServiceProvider).trackPetStream;
    return stream.where((e) => e.petId == pet.id).map(
          (t) => pet.copyWith(
            lastLocation: t.location,
          ),
        );
  },
);

/// Returns a notifier for a list of pets
final petsListNotifier = StateNotifierProvider.autoDispose((ref) {
  ref.maintainState = true;
  return PetListNotifier(
    ref: ref,
  );
});

/// A future that will resolve with list of the users pets
/// if the user has no pet it will resolve with an empty list
final petListFutureProvider = FutureProvider.autoDispose(
  (ref) => ref.read(petsListNotifier.notifier).loadList(),
);
