import 'package:find_your_pet/core/failures/failures.dart';
import 'package:find_your_pet/features/infrastructure/model/user_profile.dart';

abstract class ProfileNotifierState {
  bool get isLoading {
    return this is ProfileNotifierLoading;
  }

  bool get hasError {
    return this is ProfileNotifierError;
  }

  bool get hasData {
    return this is ProfileNotifierLoaded;
  }
}

class ProfileNotifierInitial extends ProfileNotifierState {
  ProfileNotifierInitial() : super();
}

class ProfileNotifierLoading extends ProfileNotifierState {
  ProfileNotifierLoading() : super();
}

class ProfileNotifierLoaded extends ProfileNotifierState {
  final UserProfile user;
  ProfileNotifierLoaded(this.user) : super();
}

class ProfileNotifierError extends ProfileNotifierState {
  final Failure failure;
  ProfileNotifierError(this.failure) : super();
}
