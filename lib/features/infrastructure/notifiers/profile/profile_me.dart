import 'package:find_your_pet/core/failures/failures.dart';
import 'package:find_your_pet/features/data/profile_repository.impl.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/profile/profile_notifier.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final profileNotifierState = StateNotifierProvider.autoDispose((ref) {
  ref.maintainState = true;
  return ProfileStateNotifier(
    ref: ref,
    profileRepository: ref.watch(profileProvider),
  );
});
