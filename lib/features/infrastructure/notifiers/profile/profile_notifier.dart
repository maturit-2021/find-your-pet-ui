import 'package:file_picker/file_picker.dart';
import 'package:find_your_pet/core/failures/failures.dart';
import 'package:find_your_pet/features/data/logger_repository.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/profile/profile_state.dart';
import 'package:find_your_pet/features/infrastructure/repositories/profile_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logger/logger.dart';
import 'dart:convert';

import 'profile_state.dart';

class ProfileStateNotifier extends StateNotifier<ProfileNotifierState> {
  final ProfileRepository profileRepository;
  Logger _logger;

  ProfileStateNotifier({
    required ProviderReference ref,
    required this.profileRepository,
  })  : _logger = ref.read(loggerProvider),
        super(ProfileNotifierInitial()) {
    getProfile();
  }

  Future<ProfileNotifierState> getProfile() async {
    try {
      state = ProfileNotifierLoading();
      final profile = await profileRepository.me();
      state = ProfileNotifierLoaded(profile);
    } on Failure catch (e) {
      state = ProfileNotifierError(e);
      _logger.e(e);
    } catch (e) {
      state = ProfileNotifierError(Failure.unkownError(e.toString()));
      _logger.e(e);
    }
    return state;
  }

  Future<void> updateName(String name) async {
    try {
      final oldState = state as ProfileNotifierLoaded;
      state = ProfileNotifierLoading();
      final newProfile = oldState.user.copyWith(name: name);
      await profileRepository.update(
        user: newProfile,
      );
      state = ProfileNotifierLoaded(newProfile);
    } on Failure catch (e) {
      state = ProfileNotifierError(e);
      _logger.e(e);
    } catch (e) {
      state = ProfileNotifierError(Failure.unkownError('Unkown error'));
      _logger.e(e);
    }
  }

  Future<void> updateCity(String city) async {
    try {
      final oldState = state as ProfileNotifierLoaded;
      state = ProfileNotifierLoading();
      final newProfile = oldState.user.copyWith(city: city);
      await profileRepository.update(
        user: newProfile,
      );
      state = ProfileNotifierLoaded(newProfile);
    } on Failure catch (e) {
      state = ProfileNotifierError(e);
      _logger.e(e);
    } catch (e) {
      state = ProfileNotifierError(Failure.unkownError(e.toString()));
      _logger.e(e);
    }
  }

  Future<void> updatePicure() async {
    try {
      print('ooi');
      final oldState = state as ProfileNotifierLoaded;

      FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowMultiple: false,
        withData: true,
        allowedExtensions: ['jpg', 'png'],
      );

      if (result == null) return null;

      state = ProfileNotifierLoading();

      final file = result.files.single;
      List<int> imageBytes = file.bytes!;

      await profileRepository.updatePicture(
        file: imageBytes,
        fileName: file.name!,
        user: oldState.user,
      );

      state = ProfileNotifierLoaded(oldState.user.copyWithImage(imageBytes));
    } on Failure catch (e) {
      state = ProfileNotifierError(e);
      _logger.e(e);
    } catch (e) {
      state = ProfileNotifierError(Failure.unkownError('Unkown Error'));
      _logger.e(e);
    }
  }
}
