import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logger/logger.dart';

/// Return an instance of logger that can be used to use pretty logs
final loggerProvider = Provider<Logger>((ref) => Logger());

/// Used with RiverPod to debug every state change
final loggerChangesInProvider = Provider<ProviderLogger>((ref) {
  final logger = ref.watch(loggerProvider);
  return ProviderLogger(logger);
});

class ProviderLogger extends ProviderObserver {
  ProviderLogger(this._logger);

  Logger _logger;

  @override
  void didUpdateProvider(ProviderBase provider, Object? newValue) {
    _logger.i('''
      {
        "Provider": "${provider.name}",
        "Type": "${provider.runtimeType}",
        "Value": "$newValue"
      }
    ''');
  }
}
