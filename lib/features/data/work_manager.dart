import 'dart:io';

import 'package:find_your_pet/features/data/geolocator.impl.dart';
import 'package:find_your_pet/features/data/http_repository.dart';
import 'package:find_your_pet/features/data/logger_repository.dart';
import 'package:find_your_pet/features/infrastructure/repositories/geolocator_repository.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logger/logger.dart';
import 'package:workmanager/workmanager.dart';

final workManagerProvider =
    Provider<WorkManager>((ref) => WorkManager(ref.read));

class WorkManager {
  static const String taskReadBLE = 'taskReadBLEData';
  final Reader read;
  final Logger logger;
  final GeoLocatorRepository locatorRepository;
  final HttpService httpService;

  WorkManager(this.read)
      : logger = read(loggerProvider),
        locatorRepository = read(geolocatorProvider),
        httpService = read(httpProvider) {
    _initialize();
  }

  void _initialize() {
    if (!Platform.isAndroid && !Platform.isIOS) return;

    Workmanager().initialize(
      workManagerCallbackDispatcher,
      isInDebugMode: !kReleaseMode,
    );
    logger.d('Work Manager initialized');
  }

  static Future<void> cancelTasks() {
    return Workmanager().cancelAll();
  }

  void addTask() {
    // One off task registration
    Workmanager().registerOneOffTask(
      "1",
      WorkManager.taskReadBLE,
      constraints: Constraints(
        networkType: NetworkType.connected,
        requiresBatteryNotLow: true,
        // requiresDeviceIdle: true,
      ),
      initialDelay: Duration(seconds: 5),
      existingWorkPolicy: ExistingWorkPolicy.replace,
    );
    logger.d('Task added');
  }

  Future<bool> handleBleReadEvent(String input) async {
    try {
      final position = await locatorRepository.getLocation();
      return true;
    } catch (e) {
      return false;
    }
  }
}

void workManagerCallbackDispatcher() {
  Workmanager().executeTask((task, inputData) async {
    final providerContainer = ProviderContainer();
    // switch (task) {
    //   case WorkManager.taskReadBLE:
    //     return providerContainer
    //         .read(workManagerProvider)
    //         .handleBleReadEvent(inputData);
    // }
    return Future.value(true);
  });
}
