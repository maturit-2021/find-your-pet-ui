import 'dart:async';
import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:find_your_pet/env/env.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:find_your_pet/core/failures/failures.dart';
import 'package:find_your_pet/features/data/logger_repository.dart';
import 'package:find_your_pet/features/data/utils/token_interceptor.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logger/logger.dart';

final loadingStateProvider = StateProvider<bool>((ref) => false);
final httpProvider = Provider<HttpService>((ref) {
  Logger logger = ref.read(loggerProvider);
  TokenInterceptor tokenInterceptor = ref.read(tokenInterceptorProvider);

  return HttpService(
    logger: logger,
    tokenInterceptor: tokenInterceptor,
  );
});

class HttpService {
  final _baseOptions = BaseOptions(
    baseUrl: Env.instance.baseUrl,
    contentType: "application/x-www-form-urlencoded",
  );

  late Dio _client;
  late Logger logger;
  late TokenInterceptor tokenInterceptor;

  HttpService({
    required this.logger,
    required this.tokenInterceptor,
  }) {
    _client = Dio(_baseOptions)..interceptors.add(tokenInterceptor.interceptor);

    if (!kIsWeb) {
      final adapter = (_client.httpClientAdapter as DefaultHttpClientAdapter);
      adapter.onHttpClientCreate = _configClient;
    }
  }

  Future<T?> _call<T>(Future<Response<T?>> request) async {
    try {
      final res = await request;
      if (res.toString().length > 1000)
        logger.d('Response body length ${res.toString().length}');
      else
        logger.d(res);

      return res.data;
    } on DioError catch (e) {
      logger.e(e);
      throw _handleError(e);
    } catch (e) {
      logger.e(e);
      throw Failure.unkownError();
    }
  }

  Future<T?> get<T>(
    String path, {
    Options? options,
    Map<String, dynamic>? params,
  }) {
    logger.d('Sending GET to $path, with Parameters $params');

    return _call<T?>(
      _client.get<T?>(
        path,
        queryParameters: params,
        options: options,
      ),
    );
  }

  Future<T?> post<T>(
    String path,
    dynamic data, {
    Options? options,
    Map<String, dynamic>? params,
  }) {
    logger.d('Sending Post to $path, with Parameters $params');
    logger.d('');
    logger.d('Post Body $data');

    return _call<T?>(
      _client.post<T?>(
        path,
        data: data,
        queryParameters: params,
        options: options,
      ),
    );
  }

  Future<T?> put<T>(
    String path,
    dynamic data, {
    Options? options,
    Map<String, dynamic>? params,
  }) {
    logger.d('Sending Put to $path, with Parameters $params');
    logger.d('Put Body $data');

    return _call<T?>(
      _client.put<T?>(
        path,
        data: data,
        queryParameters: params,
        options: options,
      ),
    );
  }

  Future<T?> delete<T>(
    String path,
    dynamic data, {
    Options? options,
    Map<String, dynamic>? params,
  }) {
    logger.d('Sending Delete to $path, with Parameters $params');
    logger.d('Delete Body $data');

    return _call<T?>(
      _client.delete<T>(
        path,
        data: data,
        queryParameters: params,
        options: options,
      ),
    );
  }

  /// Return the right Failure instance based on the http response status code
  Failure _handleError(DioError e) {
    logger.e(e);
    logger.e(e.response!.data);

    switch (e.response?.statusCode ?? 'none') {
      case 400:
        return Failure.badRequest("Bad formed request");
      case 401:
        return Failure.unauthorizedRequest("User not allowd");
      case 404:
        return Failure.notFound('Requested resource not found');
      default:
        return Failure.serverFailure(
            "Ops, it appears that we are having some problems, try again later.");
    }
  }

  HttpClient _configClient(HttpClient client) {
    client.badCertificateCallback = (
      X509Certificate cert,
      String host,
      int port,
    ) =>
        true;

    return client;
  }
}
