import 'package:find_your_pet/env/env.dart';
import 'package:find_your_pet/features/data/model/track_pet_event.dart';
import 'package:find_your_pet/features/infrastructure/model/general/geometry.dart';
import 'package:find_your_pet/features/infrastructure/repositories/socket_io_service.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

import 'package:find_your_pet/features/data/logger_repository.dart';
import 'package:find_your_pet/features/data/model/auth_response.dart';
import 'package:find_your_pet/features/data/token_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logger/logger.dart';
import 'package:rxdart/rxdart.dart';
import 'package:socket_io_client/socket_io_client.dart';

final socketServiceProvider =
    Provider<SocketIOService>((ref) => _SocketIOService(ref));

class _SocketIOService extends SocketIOService {
  Socket? _socket;
  late BehaviorSubject<TrackPetEvent> _bsTrackPet;

  late StateController<AuthResponse?> _tokenProvider;
  late Logger _logger;

  _SocketIOService(ProviderReference ref) {
    _logger = ref.read(loggerProvider);
    _tokenProvider = ref.read(tokenProvider);
    _bsTrackPet = BehaviorSubject();
    _tokenProvider.addListener(_handleAuthState);
    _logger.d('[Socket IO] Service created with success');
  }

  ValueStream<TrackPetEvent> get trackPetStream => _bsTrackPet.stream;

  /// Handle auth state changes and (un)subscribe to the socket service
  void _handleAuthState(AuthResponse? auth) async {
    _logger.d('Auth state has changed');
    _logger.d(auth?.toJson());

    if (auth != null && !auth.isExpired) {
      _socket = io(
        Env.instance.wsBaseUrl,
        OptionBuilder()
            .setTransports([kIsWeb ? 'polling' : 'websocket'])
            .setExtraHeaders({
              'Authorization': 'Bearer ${auth.token}',
            })
            .enableAutoConnect()
            .enableForceNew()
            .build(),
      );

      _socket?.on("connect_error", (msg) => _logger.d(msg));
      _socket?.on('disconnect', (_) => _logger.d('disconnect'));
      _socket?.emit('room');

      _socket?.on('pet-track', _handlePetTrackEvent);

      final coordinates = [
        [43.719450859097506, 10.39056582438453],
        [43.71930983161853, 10.391025823203078],
        [43.71950949929207, 10.390804540979468],
        [43.719590432358025, 10.390615445258694],
        [43.71938882643381, 10.390608739736683],
      ];
      int i = 0;
      while (true) {
        final track = TrackPetEvent(
          petId: 'e179ead1-160f-4e3e-8d23-a8bd144843a6',
          location: Geometry(
            coordinates: coordinates[i],
          ),
        );
        _bsTrackPet.add(track);
        i = (i + 1) % coordinates.length;

        await Future.delayed(const Duration(seconds: 5));
      }
    } else if (_socket != null) {
      _socket?.dispose();
      _socket = null;
    }
  }

  /// Handle pet-track event incoming from the socket
  void _handlePetTrackEvent(data) {
    try {
      _bsTrackPet.add(TrackPetEvent.fromJson(data));
      _logger.d(data);
    } catch (e) {
      _bsTrackPet.addError(e);
      _logger.e(e);
    }
  }
}
