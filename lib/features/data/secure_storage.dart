import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:localstorage/localstorage.dart';

final secureStorageProvider = Provider<Storage>((_) => _Storage());

abstract class Storage {
  Future<void> write({required String key, dynamic value});
  Future<String?> read(String key);
  Future<void> delete(String key);
}

class _Storage extends Storage {
  final _secureStorage = FlutterSecureStorage();
  final _localStorage = LocalStorage('find_your_pet');

  _Storage();

  @override
  Future<String?> read(String key) {
    if (kIsWeb) {
      return _localStorage.getItem(key);
    }
    return _secureStorage.read(key: key);
  }

  @override
  Future<void> write({required String key, value}) async {
    if (kIsWeb) {
      return _localStorage.setItem(key, value);
    }
    return _secureStorage.write(key: key, value: value);
  }

  @override
  Future<void> delete(String key) {
    if (kIsWeb) {
      return _localStorage.deleteItem(key);
    }
    return _secureStorage.delete(key: key);
  }
}
