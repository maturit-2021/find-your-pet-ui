import 'package:dio/dio.dart';
import 'package:find_your_pet/features/data/model/auth_response.dart';
import 'package:find_your_pet/features/data/token_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final tokenInterceptorProvider = Provider<TokenInterceptor>((ref) {
  return TokenInterceptor(ref.read(tokenProvider));
});

class TokenInterceptor {
  StateController<AuthResponse?> _tokenProvider;

  TokenInterceptor(this._tokenProvider);

  InterceptorsWrapper get interceptor {
    return InterceptorsWrapper(
      onRequest: (options, handler) {
        if (_tokenProvider.state != null)
          options.headers['Authorization'] =
              'Bearer ${_tokenProvider.state?.token}';
        handler.next(options);
      },
    );
  }
}
