import 'dart:convert';

import 'package:find_your_pet/core/failures/failures.dart';
import 'package:find_your_pet/features/data/model/auth_response.dart';
import 'package:find_your_pet/features/data/secure_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final tokenProvider = StateProvider<AuthResponse?>((ref) => null);

final tokenServiceProvider = Provider<TokenRepository>((ref) {
  return _TokenRepository(
    ref.read(secureStorageProvider),
    ref.read(tokenProvider),
  );
});

/// Future provider that will read the tokenServiceProvider and return the current token
final getTokenFutureProvider =
    FutureProvider.autoDispose<AuthResponse?>((ref) async {
  final tokenService = ref.read(tokenServiceProvider);
  return (await tokenService.token);
});

abstract class TokenRepository {
  Future<AuthResponse?> get token;
  Future<void> setToken(AuthResponse token);
  Future<void> clear();
}

class _TokenRepository extends TokenRepository {
  static const String tokenKey = 'JWTAuth';

  StateController<AuthResponse?> _token;
  Storage _secureStorage;

  _TokenRepository(this._secureStorage, this._token);

  Future<AuthResponse?> get token async {
    if (_token.state != null) {
      return _token.state;
    }

    String? storedToken = await _secureStorage.read(tokenKey);
    if (!kReleaseMode && storedToken == null)
      storedToken = AuthResponse.defaultValue();

    if (storedToken != null) {
      final jwtAuth = AuthResponse.fromJson(json.decode(storedToken));
      _token.state = jwtAuth;

      return jwtAuth;
    }

    final error = Failure.unauthenticated();
    _token.state = null;
    throw error;
  }

  Future<void> setToken(AuthResponse token) async {
    this._secureStorage.write(key: tokenKey, value: token.toString());
    _token.state = token;
  }

  Future<void> clear() async {
    await _secureStorage.delete(tokenKey);
    _token.state = null;
  }
}
