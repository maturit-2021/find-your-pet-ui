import 'package:find_your_pet/core/failures/failures.dart';
import 'package:find_your_pet/features/infrastructure/repositories/geolocator_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:geolocator/geolocator.dart';

final geolocatorProvider = Provider<GeoLocatorRepository>(
  (ref) => _GeoLocatorRepositoryImpl(),
);

class _GeoLocatorRepositoryImpl extends GeoLocatorRepository {
  @override
  Future<Position> getLocation() async {
    // Test if location services are enabled.
    bool serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error(Failure.gpsNotEnabled());
    }

    var permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();

      if (permission == LocationPermission.denied) {
        return Future.error(Failure.gpsDenied());
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(Failure.gpsDeniedForever());
    }

    final position = await Geolocator.getLastKnownPosition();
    if (position != null) return position;

    return await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.medium,
    );
  }
}
