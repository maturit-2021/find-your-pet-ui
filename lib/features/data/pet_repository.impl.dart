import 'package:dio/dio.dart';
import 'package:find_your_pet/features/data/logger_repository.dart';
import 'package:find_your_pet/features/infrastructure/model/pets/pet_model.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/pet/pet_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logger/logger.dart';

import 'http_repository.dart';

final petsProvider = Provider<PetRepository>(
  (ref) => _PetRepositoryImpl(
    logger: ref.read(loggerProvider),
    httpService: ref.read(httpProvider),
  ),
);

class _PetRepositoryImpl extends PetRepository {
  Logger logger;
  HttpService httpService;

  _PetRepositoryImpl({
    required this.logger,
    required this.httpService,
  });

  @override
  Future<List<PetModel>> myPets([bool forceFetch = false]) async {
    try {
      final response = await httpService.get('/pet/own');
      List<PetModel> userPets = [];

      if (response['data']?.length > 0) {
        userPets = (response['data'] as List)
            .map((m) => PetModel.fromJson(m))
            .toList();
      }
      return userPets;
    } catch (e) {
      logger.d(e);
      rethrow;
    }
  }

  @override
  Future<void> updatePicture({
    required List<int> file,
    required String fileName,
    required PetModel pet,
  }) async {
    try {
      final formData = FormData.fromMap({
        "image": MultipartFile.fromBytes(file, filename: fileName),
      });

      await httpService.put(
        '/pet/${pet.id}/picture',
        formData,
        options: Options(
          contentType: 'multipart/form-data',
        ),
      );
    } catch (e) {
      logger.d(e);
      rethrow;
    }
  }

  @override
  Future<PetModel> addPet({required String name}) async {
    final response = await httpService.post('/pet', {'name': name});
    final petModel = PetModel.fromJson(response['data']);

    return petModel;
  }

  @override
  Future<void> deletePet({required String id}) async {
    await httpService.delete('/pet', {'id': id});
  }

  @override
  void resetState() {}
}
