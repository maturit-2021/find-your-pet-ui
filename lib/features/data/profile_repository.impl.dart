import 'package:dio/dio.dart';
import 'package:find_your_pet/features/data/http_repository.dart';
import 'package:find_your_pet/features/data/logger_repository.dart';
import 'package:find_your_pet/features/infrastructure/repositories/profile_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logger/logger.dart';

import 'package:find_your_pet/features/infrastructure/model/user_profile.dart';

final profileProvider = Provider<ProfileRepository>(
  (ref) => _ProfileRepositoryImpl(
    logger: ref.read(loggerProvider),
    httpService: ref.read(httpProvider),
  ),
);

class _ProfileRepositoryImpl extends ProfileRepository {
  Logger logger;
  HttpService httpService;

  _ProfileRepositoryImpl({
    required this.logger,
    required this.httpService,
  });

  @override
  Future<UserProfile> me() async {
    try {
      logger.d('Requesting user profile');
      final response = await httpService.get('/profile');
      final userProfile = UserProfile.fromJson(response['data']);

      return userProfile;
    } catch (e) {
      logger.d(e);
      rethrow;
    }
  }

  @override
  Future<void> update({required UserProfile user}) async {
    try {
      await httpService.put('/profile', user.toMap());
    } catch (e) {
      logger.d(e);
      rethrow;
    }
  }

  @override
  Future<void> updatePicture({
    required List<int> file,
    required String fileName,
    required UserProfile user,
  }) async {
    try {
      final formData = FormData.fromMap({
        "image": MultipartFile.fromBytes(file, filename: fileName),
      });

      await httpService.put(
        '/profile/picture',
        formData,
        options: Options(
          contentType: 'multipart/form-data',
        ),
      );
    } catch (e) {
      logger.d(e);
      rethrow;
    }
  }
}
