import 'dart:convert';

import 'package:find_your_pet/core/utils/types.dart';
import 'package:find_your_pet/features/infrastructure/model/general/geometry.dart';

class TrackPetEvent {
  final String petId;
  final Geometry location;
  TrackPetEvent({
    required this.petId,
    required this.location,
  });

  TrackPetEvent copyWith({
    String? petId,
    Geometry? location,
  }) {
    return TrackPetEvent(
      petId: petId ?? this.petId,
      location: location ?? this.location,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'petId': petId,
      'location': location.toMap(),
    };
  }

  factory TrackPetEvent.fromMap(Map<String, dynamic> map) {
    return TrackPetEvent(
      petId: map['petId'],
      location: Geometry.fromMap(map),
    );
  }

  String toJson() => json.encode(toMap());

  factory TrackPetEvent.fromJson(String source) =>
      TrackPetEvent.fromMap(json.decode(source));

  @override
  String toString() => 'TrackPetEvent(petId: $petId, location: $location)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is TrackPetEvent &&
        other.petId == petId &&
        other.location == location;
  }

  @override
  int get hashCode => petId.hashCode ^ location.hashCode;
}
