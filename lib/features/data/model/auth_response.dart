import 'dart:convert';

import 'package:jwt_decode/jwt_decode.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'auth_response.freezed.dart';
part 'auth_response.g.dart';

@freezed
class AuthResponse with _$AuthResponse {
  const AuthResponse._();
  factory AuthResponse({
    required String token,
    required String refreshToken,
    required String email,
  }) = _AuthResponse;

  factory AuthResponse.fromJson(Map<String, dynamic> json) =>
      _$AuthResponseFromJson(json);

  static String defaultValue() {
    return AuthResponse(
      token:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJmNzVmMGUxZS1lOTQ3LTRlMGYtODU3ZS1jYjEwMTZkNjhlZGMiLCJwcm9maWxlSWQiOiJiN2U2NWUxYi1kZGUxLTQ4OGMtYWRjNS1jMjllYmRlZTEzNjgiLCJlbWFpbCI6ImxlYW5kcm9sZXR0b0BnbWFpbC5jb20iLCJpYXQiOjE2MjI0NzgzMDYsImV4cCI6MTYyMzA4MzEwNn0.pQ_xtSZEaq3oIytIjM1d1d8AVtZ8IFGea06QK9TRVXw",
      refreshToken:
          "eyJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkQXQiOjE2MTk4OTM1NTAxOTIsInByb2ZpbGVJZCI6MSwiaWQiOjIsImV4cCI6MTYyMDUxNjQ5NCwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIn0.uY8InxTNWF1MpzN-GsqS1Xi9IvmKuklmUXC2MpHMijc",
      email: "test@test.com",
    ).toString();
  }

  @override
  String toString() {
    return json.encode(this.toJson());
  }

  /// Check whether the current token is expired or not
  bool get isExpired => Jwt.isExpired(token);
}
