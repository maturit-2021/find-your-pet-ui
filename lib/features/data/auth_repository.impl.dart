import 'package:find_your_pet/core/failures/failures.dart';
import 'package:find_your_pet/features/data/http_repository.dart';
import 'package:find_your_pet/features/data/logger_repository.dart';
import 'package:find_your_pet/features/data/model/auth_response.dart';
import 'package:find_your_pet/features/data/profile_repository.impl.dart';
import 'package:find_your_pet/features/data/token_repository.dart';
import 'package:find_your_pet/features/infrastructure/model/user_profile.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/auth/login/login_state.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/auth/register/register_state.dart';
import 'package:find_your_pet/features/infrastructure/repositories/auth_repository.dart';
import 'package:find_your_pet/features/infrastructure/repositories/profile_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logger/logger.dart';

final authProvider = Provider<AuthRepository>(
  (ref) => _AuthRepositoryImpl(
    logger: ref.read(loggerProvider),
    httpService: ref.read(httpProvider),
    tokenRepository: ref.read(tokenServiceProvider),
    profileRepository: ref.read(profileProvider),
  ),
);

class _AuthRepositoryImpl extends AuthRepository {
  Logger logger;
  HttpService httpService;
  TokenRepository tokenRepository;
  ProfileRepository profileRepository;

  _AuthRepositoryImpl({
    required this.logger,
    required this.httpService,
    required this.tokenRepository,
    required this.profileRepository,
  });

  @override
  Future<UserProfile> authenticate(LoginParams params) async {
    try {
      var response = await httpService.post('/auth/login', params.toJson());
      var authResponse = AuthResponse.fromJson(response['data']);
      logger.d(authResponse.toJson());

      await tokenRepository.setToken(authResponse);

      return profileRepository.me();
    } on UnauthorizedRequest catch (_) {
      throw Failure.unauthorizedRequest(
          'Invalid credentials, please try again!');
    }
  }

  @override
  Future<bool> checkAuthentication() async {
    var token = await tokenRepository.token;
    if (token == null) return false;

    if (token.isExpired) {
      var response = await httpService.post('/auth/refresh', token.toJson());
      var authResponse = AuthResponse.fromJson(response['data']);
      await tokenRepository.setToken(authResponse);
    }

    return true;
  }

  @override
  Future<UserProfile> register(RegisterProfileParams params) async {
    var response = await httpService.post('/auth/register', params.toJson());
    var authResponse = AuthResponse.fromJson(response['data']);
    logger.d(authResponse.toJson());

    await tokenRepository.setToken(authResponse);

    return profileRepository.me();
  }

  @override
  Future<void> logout() async {
    await tokenRepository.clear();
  }
}
