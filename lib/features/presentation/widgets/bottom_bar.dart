import 'package:find_your_pet/features/presentation/widgets/empty_widget.dart';
import 'package:find_your_pet/features/presentation/widgets/navigation_tab.dart';
import 'package:find_your_pet/features/presentation/widgets/response_layout.dart';
import 'package:flutter/material.dart';

class BottomBar extends StatelessWidget implements PreferredSizeWidget {
  final TabController controller;

  const BottomBar({
    Key? key,
    required this.controller,
    this.preferredSize = const Size.fromHeight(60.0),
  }) : super(key: key);

  @override
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return ResponsiveLayout(
      pc: EmptyWidget(),
      tablet: EmptyWidget(),
      mobile: Container(
        decoration: BoxDecoration(
          color: theme.colorScheme.surface,
          boxShadow: [
            BoxShadow(
              color: theme.colorScheme.onBackground.withOpacity(0.25),
              spreadRadius: 0,
              blurRadius: 16,
              offset: const Offset(0, -2),
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 7),
          child: NavigationTab(
            controller: controller,
          ),
        ),
      ),
    );
  }
}
