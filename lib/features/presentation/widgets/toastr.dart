import 'package:flutter/material.dart';

class Toastr extends InheritedWidget {
  static late BuildContext _context;

  const Toastr({
    Key? key,
    required Widget child,
  }) : super(key: key, child: child);

  static Toastr? of(BuildContext context) {
    _context = context;
    return context.dependOnInheritedWidgetOfExactType<Toastr>();
  }

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) {
    return false;
  }

  /// Show an snackbar with an error message
  /// The style is already predefined using the
  /// colorscheme of the current context
  void error(String text) {
    final scaffold = ScaffoldMessenger.of(_context);
    final theme = Theme.of(_context);
    scaffold.clearSnackBars();
    Future.microtask(
      () => scaffold.showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          backgroundColor: theme.colorScheme.error,
          content: Text(
            text,
            style: theme.textTheme.bodyText2!.copyWith(
              color: theme.colorScheme.onError,
            ),
          ),
          action: SnackBarAction(
            label: 'Close',
            textColor: theme.colorScheme.onError,
            onPressed: scaffold.hideCurrentSnackBar,
          ),
        ),
      ),
    );
  }

  void success(String text) {
    final scaffold = ScaffoldMessenger.of(_context);
    final theme = Theme.of(_context);
    scaffold.clearSnackBars();

    Future.microtask(
      () => scaffold.showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          backgroundColor: theme.colorScheme.secondary,
          content: Text(
            text,
            style: theme.textTheme.bodyText2!.copyWith(
              color: theme.colorScheme.onError,
            ),
          ),
          action: SnackBarAction(
            label: 'Close',
            textColor: theme.colorScheme.onError,
            onPressed: scaffold.hideCurrentSnackBar,
          ),
        ),
      ),
    );
  }
}
