import 'package:find_your_pet/features/presentation/widgets/navigation_tab.dart';
import 'package:find_your_pet/features/presentation/widgets/response_layout.dart';
import 'package:find_your_pet/features/presentation/widgets/user_avatar.dart';
import 'package:flutter/material.dart';

import 'logo.dart';

class AppHeader extends StatelessWidget implements PreferredSizeWidget {
  final TabController controller;

  const AppHeader({
    Key? key,
    required this.controller,
    this.preferredSize = const Size.fromHeight(60.0),
  }) : super(key: key);

  @override
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    final userAvatar = UserAvatar();
    final theme = Theme.of(context);

    return Container(
      decoration: BoxDecoration(
        color: theme.colorScheme.surface,
        boxShadow: [
          BoxShadow(
            color: theme.colorScheme.onBackground.withOpacity(0.25),
            spreadRadius: 0,
            blurRadius: 16,
            offset: const Offset(0, 2),
          ),
        ],
      ),
      child: ResponsiveLayout(
        pc: buildPcLayout(userAvatar, theme),
        tablet: buildPcLayout(userAvatar, theme),
        mobile: buildMobileLayout(userAvatar),
      ),
    );
  }

  Widget buildPcLayout(UserAvatar userAvatar, ThemeData theme) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: ResponsiveLayout.kHorizontalPadding,
        vertical: ResponsiveLayout.kVerticalPadding / 3,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Logo(),
          NavigationTab(
            controller: controller,
          ),
          userAvatar,
        ],
      ),
    );
  }

  Widget buildMobileLayout(UserAvatar userAvatar) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: ResponsiveLayout.kHorizontalPadding / 3,
        vertical: ResponsiveLayout.kVerticalPadding / 3,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Logo(),
          userAvatar,
        ],
      ),
    );
  }
}
