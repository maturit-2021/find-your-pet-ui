import 'package:find_your_pet/features/presentation/widgets/response_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class NavigationTab extends HookWidget {
  final TabController controller;
  const NavigationTab({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final notifier = useState(controller.index);
    final theme = Theme.of(context);
    final isMobile = ResponsiveLayout.isPhone(context);
    final iconSize = isMobile ? 35.0 : null;
    final sizedSpaceX = SizedBox(
      width: isMobile ? 60 : 20,
    );

    return Row(
      mainAxisAlignment:
          isMobile ? MainAxisAlignment.center : MainAxisAlignment.spaceBetween,
      children: [
        InkWell(
          onTap: () => _handleIconClick(0, notifier),
          child: Icon(
            notifier.value == 0 ? Icons.home : Icons.home_outlined,
            size: iconSize,
            color: notifier.value == 0 ? theme.colorScheme.primary : null,
          ),
        ),
        sizedSpaceX,
        InkWell(
          onTap: () => _handleIconClick(1, notifier),
          child: Icon(
            notifier.value == 1 ? Icons.map : Icons.map_outlined,
            size: iconSize,
            color: notifier.value == 1 ? theme.colorScheme.primary : null,
          ),
        ),
        sizedSpaceX,
        InkWell(
          onTap: () => _handleIconClick(2, notifier),
          child: Icon(
            notifier.value == 2 ? Icons.pets : Icons.pets_outlined,
            size: iconSize,
            color: notifier.value == 2 ? theme.colorScheme.primary : null,
          ),
        ),
        sizedSpaceX,
        InkWell(
          onTap: () => _handleIconClick(3, notifier),
          child: Icon(
            notifier.value == 3 ? Icons.settings : Icons.settings_outlined,
            size: iconSize,
            color: notifier.value == 3 ? theme.colorScheme.primary : null,
          ),
        ),
      ],
    );
  }

  void _handleIconClick(int index, ValueNotifier<int> notifier) {
    controller.animateTo(index);
    notifier.value = index;
  }
}
