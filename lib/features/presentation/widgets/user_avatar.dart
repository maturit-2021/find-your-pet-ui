import 'package:find_your_pet/features/infrastructure/model/user_profile.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/profile/profile_me.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/profile/profile_state.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class UserAvatar extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final notifier = watch(profileNotifierState);

    if (notifier is ProfileNotifierLoaded)
      return buildProfile(context, notifier.user);

    return Container();
  }

  Widget buildProfile(BuildContext context, UserProfile profile) {
    final theme = Theme.of(context);
    dynamic image = profile.image != null
        ? MemoryImage(profile.image!)
        : AssetImage('assets/images/profile.jpg');

    return InkWell(
      onTap: () => null,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            decoration: BoxDecoration(
              color: theme.colorScheme.surface,
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  blurRadius: 2,
                  color: theme.colorScheme.onBackground,
                  spreadRadius: 0,
                )
              ],
            ),
            child: CircleAvatar(
              radius: 30,
              backgroundImage: image,
            ),
          ),
        ],
      ),
    );
  }
}
