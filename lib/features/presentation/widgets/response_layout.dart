import 'package:flutter/material.dart';
import 'dart:math' as math;

class ResponsiveLayout extends StatelessWidget {
  final Widget? phone, tablet, mobile;
  final Widget pc;

  static int phoneBp = 640;
  static int tabletBp = 1200;

  static double kMaxContentSize = 1024.0;

  static double kVerticalPadding = 30.0;
  static double kHorizontalPadding = 50.0;

  const ResponsiveLayout({
    Key? key,
    this.phone,
    this.tablet,
    this.mobile,
    required this.pc,
  }) : super(key: key);

  static bool isPhone(BuildContext context) =>
      MediaQuery.of(context).size.width < phoneBp;

  static bool isTablet(BuildContext context) =>
      MediaQuery.of(context).size.width < tabletBp &&
      MediaQuery.of(context).size.width > phoneBp;

  static bool isPc(BuildContext context) =>
      !isPhone(context) && !isTablet(context);

  /// Determinate the horizontal padding to be applied
  /// based on the current device width, if running on
  /// small devices it will return `18.0`, otherwise
  /// it will return `1/4` of the current Device Width with minimum of
  /// `preferredMin`  default is to `500`
  static double hPadding(BuildContext context, [int preferredMin = 500]) {
    final sizeW = MediaQuery.of(context).size.width;
    return isPc(context) ? math.min(sizeW / 4, preferredMin).toDouble() : 18.0;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        if (isPhone(context))
          return this.phone ?? this.mobile ?? this.pc;
        else if (isTablet(context))
          return this.tablet ?? this.mobile ?? this.pc;
        else
          return this.pc;
      },
    );
  }
}
