import 'package:flutter/material.dart';

class CircularLoader extends StatelessWidget {
  final Color? color;

  const CircularLoader({Key? key, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation(color ?? theme.colorScheme.primary),
      strokeWidth: 2.0,
      semanticsLabel: "Loading",
    );
  }
}
