import 'package:flutter/material.dart';

class PostCard extends StatelessWidget {
  const PostCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Card(
      borderOnForeground: false,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          buildCardHeader(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            child: Text(
              'This is a fake description for a fake post to test how to display information',
              textAlign: TextAlign.left,
              style: theme.textTheme.bodyText2,
            ),
          ),
          FractionallySizedBox(
            widthFactor: 1.0,
            child: Image.asset(
              'assets/images/cat.jpg',
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(
            height: 40,
          ),
        ],
      ),
    );
  }

  Widget buildCardHeader() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CircleAvatar(
            radius: 24,
            backgroundImage: AssetImage('assets/images/profile.jpg'),
          ),
          const SizedBox(width: 20),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Fake account'),
                Text('Today'),
              ],
            ),
          ),
          Icon(
            Icons.more_vert,
            size: 20,
          )
        ],
      ),
    );
  }
}
