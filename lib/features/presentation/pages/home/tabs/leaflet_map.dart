import 'package:find_your_pet/features/infrastructure/model/pets/pet_model.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/geolocator/get_location.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/pet/pet_providers.dart';
import 'package:find_your_pet/features/presentation/widgets/circular_loader.dart';
import 'package:find_your_pet/features/presentation/widgets/empty_widget.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:latlong2/latlong.dart';

class LeafLetMap extends StatefulWidget {
  LeafLetMap({Key? key}) : super(key: key);

  @override
  _LeafLetMapState createState() => _LeafLetMapState();
}

class _LeafLetMapState extends State<LeafLetMap> {
  final MapController _mapController = MapController();

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, watch, __) {
        final locationFuture = watch(getLocationFutureProvider);

        return locationFuture.maybeWhen(
          data: (position) {
            _mapController.move(
              LatLng(
                position.latitude,
                position.longitude,
              ),
              _mapController.zoom,
            );
            return _buildMap();
          },
          orElse: () => _buildMap(),
        );
      },
    );
  }

  Widget _buildMap() {
    return Listener(
      onPointerSignal: _handlePointSignal,
      child: Consumer(
        builder: (context, watch, _) {
          final stream = watch(petListFutureProvider);
          return stream.when(
            data: (data) => FlutterMap(
              mapController: _mapController,
              options: _mapOptions(),
              children: [
                _tileLayerWidget(),
                ...List.generate(
                  data.length,
                  (i) => _buildPetMarker(data[i]),
                ),
              ],
            ),
            loading: () => const Center(
              child: SizedBox(
                width: 64,
                height: 64,
                child: CircularLoader(),
              ),
            ),
            error: (e, _) => Center(
              child: Text('Unkown Error'),
            ),
          );
        },
      ),
    );
  }

  TileLayerWidget _tileLayerWidget() {
    return TileLayerWidget(
      options: TileLayerOptions(
        urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        subdomains: const ['a', 'b', 'c'],
      ),
    );
  }

  MapOptions _mapOptions() {
    return MapOptions(
      zoom: 13.0,
      maxZoom: 18.0,
      interactiveFlags: InteractiveFlag.all,
      enableMultiFingerGestureRace: true,
    );
  }

  Consumer _buildPetMarker(PetModel pet) {
    return Consumer(
      builder: (context, watch, _) {
        final petStrack = watch(petTrackStream(pet));

        return petStrack.when(
          data: (pet) {
            return MarkerLayerWidget(
              options: MarkerLayerOptions(
                markers: [
                  Marker(
                    width: 80.0,
                    height: 80.0,
                    point: pet.lastLocation!.latLong,
                    builder: (ctx) => Container(
                      child: GestureDetector(
                        onTap: () =>
                            _mapController.move(pet.lastLocation!.latLong, 18),
                        child: CircleAvatar(
                          radius: 40,
                          backgroundImage: _buildPetImage(pet),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            );
          },
          loading: () => const EmptyWidget(),
          error: (e, _) => const EmptyWidget(),
        );
      },
    );
  }

  ImageProvider _buildPetImage(PetModel pet) {
    if (pet.image != null) {
      return MemoryImage(
        pet.petImage!,
      );
    }

    return AssetImage(
      'assets/images/pet-profile.jpg',
    );
  }

  void _handlePointSignal(PointerSignalEvent pointerSignal) {
    if (pointerSignal is PointerScrollEvent) {
      if (pointerSignal.scrollDelta.dy < 0) {
        _mapController.move(_mapController.center, _mapController.zoom + 1);
      } else {
        _mapController.move(_mapController.center, _mapController.zoom - 1);
      }
    }
  }
}
