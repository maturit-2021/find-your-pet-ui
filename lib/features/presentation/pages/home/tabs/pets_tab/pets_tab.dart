import 'package:find_your_pet/core/failures/failures.dart';
import 'package:find_your_pet/features/infrastructure/model/pets/pet_model.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/pet/pet_list_state.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/pet/pet_providers.dart';
import 'package:find_your_pet/features/presentation/pages/home/tabs/pets_tab/pet_card.dart';
import 'package:find_your_pet/features/presentation/pages/input/input_page.dart';
import 'package:find_your_pet/features/presentation/widgets/circular_loader.dart';
import 'package:find_your_pet/features/presentation/widgets/response_layout.dart';
import 'package:find_your_pet/features/presentation/widgets/toastr.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class PetsTab extends ConsumerWidget {
  const PetsTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, watch) {
    final listPets = watch(petListFutureProvider);

    return Scaffold(
      body: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: Container(
          padding: EdgeInsets.only(
            left: 18,
            right: 18,
            top: ResponsiveLayout.kVerticalPadding,
          ),
          child: listPets.when(
            data: buildContent,
            loading: buildLoading,
            error: (e, __) => Text(e.toString()),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _addPetInput(context),
        tooltip: 'Add new PET',
        child: Icon(Icons.add),
      ),
    );
  }

  Widget buildContent(List<PetModel> p) {
    return Consumer(
      builder: (context, watch, _) {
        final state = watch(petsListNotifier);

        if (state is PetListLoading) return buildLoading();
        if (state is PetListError) Toastr.of(context)!.error(state.message);
        final pets = state.pets;

        return Wrap(
          spacing: 10,
          runSpacing: 10,
          children: List.generate(
            pets.length,
            (index) => PetCard(
              pet: pets[index],
            ),
          ),
        );
      },
    );
  }

  Widget buildLoading() {
    return Align(
      alignment: Alignment.center,
      child: SizedBox(
        width: 64,
        height: 64,
        child: CircularLoader(),
      ),
    );
  }

  void _addPetInput(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return InputPage(
            buttonTitle: 'Add',
            labelText: 'Insert your pet name',
            onSend: (input) async {
              Navigator.of(context).pop();
              await context.read(petsListNotifier.notifier).addPet(input);
            },
          );
        },
      ),
    );
  }
}
