import 'package:find_your_pet/features/infrastructure/model/pets/pet_model.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/pet/pet_providers.dart';
import 'package:find_your_pet/features/presentation/widgets/response_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class PetCard extends ConsumerWidget {
  final PetModel pet;

  const PetCard({
    Key? key,
    required this.pet,
  }) : super(key: key);

  @override
  Widget build(context, watch) {
    final wSize = MediaQuery.of(context).size.width;
    double? cWidth = !ResponsiveLayout.isPc(context) ? null : wSize * 0.25;

    return SizedBox(
      width: cWidth,
      child: Card(
        borderOnForeground: false,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            buildCardHeader(),
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: FractionallySizedBox(
                widthFactor: 1.0,
                child: _buildPetImage(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildCardHeader() {
    return Builder(builder: (context) {
      final theme = Theme.of(context);
      final popItemStyle = theme.textTheme.bodyText2;

      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(width: 20),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    pet.name,
                    style: theme.textTheme.bodyText1!.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    pet.lastSeen ?? '',
                    style: theme.textTheme.subtitle1,
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 50,
              child: PopupMenuButton<String>(
                child: Icon(
                  Icons.more_vert,
                  size: 32,
                ),
                itemBuilder: (context) => [
                  PopupMenuItem(
                    child: GestureDetector(
                      onTap: () async {
                        Navigator.pop(context);
                        await context
                            .read(petsListNotifier.notifier)
                            .pickImage(pet);
                      },
                      child: Text(
                        "Change picture",
                        style: popItemStyle,
                      ),
                    ),
                  ),
                  PopupMenuItem(
                    child: GestureDetector(
                      onTap: () => null,
                      child: Text(
                        "Set status",
                        style: popItemStyle,
                      ),
                    ),
                  ),
                  PopupMenuItem(
                    child: GestureDetector(
                      onTap: () async {
                        Navigator.pop(context);
                        await context
                            .read(petsListNotifier.notifier)
                            .deletePet(pet);
                      },
                      child: Text(
                        "Delete",
                        style: popItemStyle,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );
    });
  }

  Widget _buildPetImage() {
    if (pet.image != null) {
      return Image.memory(
        pet.petImage!,
        fit: BoxFit.fill,
      );
    }

    return Image.asset(
      'assets/images/pet-profile.jpg',
      fit: BoxFit.fill,
    );
  }

  // void _showToast(BuildContext context, String text) {
  //   final scaffold = ScaffoldMessenger.of(context);
  //   scaffold.showSnackBar(
  //     SnackBar(
  //       content: Text(text),
  //       action: SnackBarAction(
  //         label: '',
  //         onPressed: scaffold.hideCurrentSnackBar,
  //       ),
  //     ),
  //   );
  // }
}
