import 'package:find_your_pet/features/presentation/widgets/card.dart';
import 'package:find_your_pet/features/presentation/widgets/response_layout.dart';
import 'package:flutter/material.dart';

class HomeTab extends StatelessWidget {
  const HomeTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final padding = ResponsiveLayout.hPadding(context);

    return Container(
      padding: EdgeInsets.only(left: padding, right: padding),
      child: ListView(
        scrollDirection: Axis.vertical,
        padding: EdgeInsets.all(ResponsiveLayout.kHorizontalPadding / 3),
        shrinkWrap: true,
        children: List.filled(
          20,
          PostCard(),
        ),
      ),
    );
  }
}
