import 'package:find_your_pet/features/infrastructure/model/user_profile.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/profile/profile_me.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/profile/profile_state.dart';
import 'package:find_your_pet/features/presentation/pages/home/tabs/config_tab/widgets/config_label.dart';
import 'package:find_your_pet/features/presentation/pages/home/tabs/config_tab/widgets/profile_header.dart';
import 'package:find_your_pet/features/presentation/widgets/circular_loader.dart';
import 'package:find_your_pet/features/presentation/widgets/response_layout.dart';
import 'package:find_your_pet/features/presentation/widgets/toastr.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class ConfigTab extends ConsumerWidget {
  const ConfigTab({Key? key}) : super(key: key);

  @override
  Widget build(context, watch) {
    final notifier = watch(profileNotifierState);
    final padding = ResponsiveLayout.hPadding(context);

    if (notifier is ProfileNotifierError)
      Toastr.of(context)?.error(notifier.failure.message ?? 'Unkown error');

    if (!ResponsiveLayout.isPc(context))
      Toastr.of(context)
          ?.error("This page hasn't been optimized for small devices yet.");

    return Container(
      padding: EdgeInsets.symmetric(horizontal: padding, vertical: 25),
      child: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: notifier is ProfileNotifierLoaded
            ? _buildContent(notifier.user)
            : _buildLoading(),
      ),
    );
  }

  Widget _buildContent(UserProfile profile) {
    return Builder(builder: (context) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ProfileHeaderConfig(
            profile: profile,
          ),
          const SizedBox(height: 25),
          Card(
            borderOnForeground: false,
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  ConfigLabel(
                    title: 'Anonymous mode',
                    caption:
                        'If enabled, your profile will not be associated with any geo location on the server',
                    onChange: (value) => Toastr.of(context)
                        ?.error("This functionality it's no activated yet"),
                  ),
                  const SizedBox(height: 20),
                  ConfigLabel(
                    title: 'Enable notifications',
                    onChange: (value) => Toastr.of(context)
                        ?.error("This functionality it's no activated yet"),
                  ),
                ],
              ),
            ),
          ),
        ],
      );
    });
  }

  Widget _buildLoading() {
    return Align(
      alignment: Alignment.center,
      child: SizedBox(
        width: 64,
        height: 64,
        child: CircularLoader(),
      ),
    );
  }
}
