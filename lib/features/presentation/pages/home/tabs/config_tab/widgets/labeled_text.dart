import 'package:flutter/material.dart';

class LabeledText extends StatelessWidget {
  const LabeledText({
    Key? key,
    required this.label,
    required this.text,
    this.isEditable = false,
    this.onEdit,
  }) : super(key: key);
  final String label, text;
  final bool isEditable;
  final Function()? onEdit;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              label,
              style: theme.textTheme.caption?.copyWith(
                color: theme.colorScheme.onBackground.withOpacity(0.5),
              ),
            ),
            if (isEditable)
              IconButton(
                icon: const Icon(Icons.edit, size: 18),
                splashRadius: 18,
                onPressed: () => this.onEdit?.call(),
              ),
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        Text(
          text,
          style: theme.textTheme.bodyText1,
        ),
      ],
    );
  }
}
