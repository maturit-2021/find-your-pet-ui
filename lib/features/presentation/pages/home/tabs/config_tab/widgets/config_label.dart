import 'package:flutter/material.dart';
import 'package:rolling_switch/rolling_switch.dart';

class ConfigLabel extends StatelessWidget {
  final String title;
  final String? caption;
  final Function(bool)? onChange;

  const ConfigLabel({
    Key? key,
    required this.title,
    this.caption,
    this.onChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: theme.textTheme.bodyText1,
            ),
            if (caption != null) const SizedBox(height: 5),
            if (caption != null)
              Text(
                caption!,
                style: theme.textTheme.caption?.copyWith(
                  color: theme.colorScheme.onBackground.withOpacity(0.5),
                ),
              ),
          ],
        ),
        MouseRegion(
          cursor: SystemMouseCursors.click,
          child: RollingSwitch.icon(
            width: 100,
            rollingInfoLeft: RollingIconInfo(
              icon: Icons.close,
              text: Text(
                'Off',
                style: theme.textTheme.bodyText2?.copyWith(
                  color: theme.colorScheme.onError,
                ),
              ),
              backgroundColor: theme.colorScheme.error,
            ),
            rollingInfoRight: RollingIconInfo(
              text: Text(
                'On',
                style: theme.textTheme.bodyText2?.copyWith(
                  color: theme.colorScheme.onError,
                ),
              ),
              backgroundColor: theme.colorScheme.primary,
            ),
            onChanged: (status) => onChange?.call(status),
          ),
        )
      ],
    );
  }
}
