import 'package:find_your_pet/features/infrastructure/model/user_profile.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/profile/profile_me.dart';
import 'package:find_your_pet/features/presentation/pages/home/tabs/config_tab/widgets/labeled_text.dart';
import 'package:find_your_pet/features/presentation/pages/input/input_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ProfileHeaderConfig extends ConsumerWidget {
  final UserProfile profile;
  const ProfileHeaderConfig({
    Key? key,
    required this.profile,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return _buildCard();
  }

  Builder _buildCard() {
    const sizedBox = const SizedBox(height: 25);

    return Builder(
      builder: (context) {
        final theme = Theme.of(context);

        return Card(
          borderOnForeground: false,
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 250,
                  child: Stack(
                    children: [
                      _buildProfileImage(),
                      Positioned.fill(
                        child: InkWell(
                          onTap: () => context
                              .read(profileNotifierState.notifier)
                              .updatePicure(),
                          child: Container(
                            color:
                                theme.colorScheme.onBackground.withOpacity(0.5),
                            child: Center(
                              child: Icon(
                                Icons.camera_alt_outlined,
                                size: 64,
                                color: theme.colorScheme.surface,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      LabeledText(
                        label: 'ID',
                        text: profile.id,
                      ),
                      sizedBox,
                      LabeledText(
                        label: 'Name',
                        text: profile.name,
                        isEditable: true,
                        onEdit: () => _askUserName(context),
                      ),
                      sizedBox,
                      LabeledText(
                        label: 'City',
                        text: profile.city,
                        isEditable: true,
                        onEdit: () => _askUserCity(context),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Image _buildProfileImage() {
    if (profile.image != null)
      return Image.memory(
        profile.image!,
        fit: BoxFit.fitWidth,
        semanticLabel: 'Your profile picture',
      );
    return Image.asset(
      'assets/images/profile.jpg',
      fit: BoxFit.fill,
    );
  }

  void _askUserName(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return InputPage(
            buttonTitle: 'Save',
            labelText: 'Type your name',
            onSend: (input) async {
              Navigator.of(context).pop();
              context.read(profileNotifierState.notifier).updateName(input);
            },
          );
        },
      ),
    );
  }

  void _askUserCity(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return InputPage(
            buttonTitle: 'Save',
            labelText: 'Type your city',
            onSend: (input) async {
              Navigator.of(context).pop();
              context.read(profileNotifierState.notifier).updateCity(input);
            },
          );
        },
      ),
    );
  }
}
