import 'package:find_your_pet/features/presentation/pages/home/tabs/config_tab/config_tab.dart';
import 'package:find_your_pet/features/presentation/pages/home/tabs/home_tab.dart';
import 'package:find_your_pet/features/presentation/pages/home/tabs/leaflet_map.dart';
import 'package:find_your_pet/features/presentation/pages/home/tabs/pets_tab/pets_tab.dart';
import 'package:find_your_pet/features/presentation/widgets/app_header.dart';
import 'package:find_your_pet/features/presentation/widgets/bottom_bar.dart';
import 'package:find_your_pet/features/presentation/widgets/response_layout.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  late TabController _controller;

  final List<Widget> _childrenTabs = [
    HomeTab(),
    LeafLetMap(),
    PetsTab(),
    ConfigTab(),
  ];

  @override
  void initState() {
    super.initState();

    _controller = TabController(vsync: this, length: _childrenTabs.length);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ResponsiveLayout(
        pc: _buildScaffoldLarge(),
        tablet: _buildScaffoldLarge(),
        phone: _buildScaffoldMobile(),
      ),
    );
  }

  Widget _buildScaffoldLarge() {
    return Scaffold(
      appBar: AppHeader(
        controller: _controller,
      ),
      body: DefaultTabController(
        length: _childrenTabs.length,
        child: TabBarView(
          controller: _controller,
          physics: NeverScrollableScrollPhysics(),
          children: _childrenTabs,
        ),
      ),
    );
  }

  Widget _buildScaffoldMobile() {
    return Scaffold(
      appBar: AppHeader(
        controller: _controller,
      ),
      body: DefaultTabController(
        length: _childrenTabs.length,
        child: TabBarView(
          controller: _controller,
          physics: NeverScrollableScrollPhysics(),
          children: _childrenTabs,
        ),
      ),
      bottomNavigationBar: BottomBar(
        controller: _controller,
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
