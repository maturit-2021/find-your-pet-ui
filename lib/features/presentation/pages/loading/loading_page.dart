import 'package:find_your_pet/features/infrastructure/notifiers/auth/login/login_state_notifier.dart';
import 'package:find_your_pet/features/presentation/widgets/circular_loader.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

/// Check if there's a valid authentication for the current device
/// if so it will redirect the user to the home page otherwise to the login
/// page
class LoadingPage extends ConsumerWidget {
  const LoadingPage({Key? key}) : super(key: key);

  @override
  Widget build(context, watch) {
    final checkLogin = watch(checkAuthProvider);

    return checkLogin.when(
      data: (isLogged) => _redirect(context, (isLogged ? '/home' : '/login')),
      error: (_, __) => _redirect(context, '/login'),
      loading: () => _buildLoading(),
    );
  }

  Widget _buildLoading() {
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: Center(
            child: CircularLoader(),
          ),
        ),
      ),
    );
  }

  Widget _redirect(BuildContext context, String url) {
    Future.microtask(
      () => Navigator.pushNamedAndRemoveUntil(context, url, (_) => false),
    );
    return const SizedBox();
  }
}
