import 'dart:math' as math;

import 'package:find_your_pet/features/infrastructure/notifiers/auth/register/register_state.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/auth/register/register_state_notifier.dart';
import 'package:find_your_pet/features/presentation/widgets/circular_loader.dart';
import 'package:find_your_pet/features/presentation/widgets/empty_widget.dart';
import 'package:find_your_pet/features/presentation/widgets/response_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:find_your_pet/core/extensions/stringX.dart';

class RegisterPage extends HookWidget {
  final _formKey = GlobalKey<FormState>();

  RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formContainer = buildFormContainer(context);
    final size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        body: ResponsiveLayout(
          mobile: formContainer,
          pc: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Padding(
                  padding: EdgeInsets.only(
                    left: ResponsiveLayout.kVerticalPadding,
                  ),
                  child: formContainer,
                ),
              ),
              ResponsiveLayout(
                pc: Align(
                  alignment: Alignment.bottomRight,
                  child: SizedBox(
                    height: size.height * 0.75,
                    child: _buildDogImage(),
                  ),
                ),
                mobile: Container(),
              )
            ],
          ),
        ),
      ),
    );
  }

  Container buildFormContainer(BuildContext context) {
    final theme = Theme.of(context);
    final size = MediaQuery.of(context).size;
    final isPc = ResponsiveLayout.isPc(context);

    return Container(
      width: isPc ? size.width * 0.35 : null,
      height: isPc ? null : size.height,
      decoration: BoxDecoration(
        color: theme.colorScheme.surface,
        borderRadius: const BorderRadius.all(Radius.circular(4.0)),
        boxShadow: [
          BoxShadow(
            color: theme.colorScheme.onBackground.withOpacity(1),
            spreadRadius: 0,
            blurRadius: 8,
            offset: const Offset(0, 4),
          ),
        ],
      ),
      child: Padding(
        padding: EdgeInsets.all(
          ResponsiveLayout.kHorizontalPadding,
        ),
        child: buildRegisterForm(context),
      ),
    );
  }

  Form buildRegisterForm(BuildContext context) {
    final theme = Theme.of(context);
    final emailController =
        useTextEditingController.fromValue(TextEditingValue.empty);
    final passwordController =
        useTextEditingController.fromValue(TextEditingValue.empty);
    final nameController =
        useTextEditingController.fromValue(TextEditingValue.empty);

    final cityController =
        useTextEditingController.fromValue(TextEditingValue.empty);

    return Form(
      key: _formKey,
      child: Scrollbar(
        isAlwaysShown: false,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(
                controller: nameController,
                style: theme.textTheme.bodyText1,
                keyboardType: TextInputType.name,
                decoration: InputDecoration(
                  isDense: true,
                  labelText: 'Name',
                  labelStyle: theme.textTheme.bodyText1,
                  contentPadding:
                      const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Required field';
                  }
                  return null;
                },
              ),
              const SizedBox(
                height: 30,
              ),
              TextFormField(
                controller: cityController,
                style: theme.textTheme.bodyText1,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  isDense: true,
                  labelText: 'City',
                  labelStyle: theme.textTheme.bodyText1,
                  contentPadding:
                      const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Required field';
                  }
                  return null;
                },
              ),
              const SizedBox(
                height: 30,
              ),
              TextFormField(
                controller: emailController,
                style: theme.textTheme.bodyText1,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  isDense: true,
                  labelText: 'Email',
                  labelStyle: theme.textTheme.bodyText1,
                  contentPadding:
                      const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Required field';
                  } else if (!value.isValidEmail()) {
                    return 'Invalid email address';
                  }
                  return null;
                },
              ),
              const SizedBox(
                height: 30,
              ),
              TextFormField(
                obscureText: true,
                controller: passwordController,
                style: theme.textTheme.bodyText1,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  isDense: true,
                  labelText: 'Password',
                  labelStyle: theme.textTheme.bodyText1,
                  contentPadding:
                      const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Invalid password';
                  }
                  return null;
                },
              ),
              const SizedBox(
                height: 15,
              ),
              _buildErrorMessage(),
              const SizedBox(
                height: 15,
              ),
              buildRegisterButton(context, emailController, passwordController,
                  nameController, cityController),
              const SizedBox(
                height: 50,
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: InkWell(
                  onTap: () => Navigator.pushNamedAndRemoveUntil(
                      context, '/login', (_) => false),
                  child: Text(
                    'Go back to Login',
                    style: theme.textTheme.bodyText2,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Consumer buildRegisterButton(
    BuildContext context,
    TextEditingController email,
    TextEditingController pass,
    TextEditingController name,
    TextEditingController city,
  ) {
    final theme = Theme.of(context);
    final media = MediaQuery.of(context);

    final textWidget = Text(
      'Register',
      style: theme.textTheme.bodyText1?.copyWith(
        color: theme.colorScheme.onError,
        fontWeight: FontWeight.bold,
      ),
    );

    return Consumer(
      builder: (BuildContext context, watch, __) {
        final state = watch(registerStateNotifier);
        final isLoading = state is RegisterLoading;
        if (state is RegisterLoaded) {
          Future.microtask(
            () => Navigator.pushNamedAndRemoveUntil(
              context,
              '/home',
              (_) => false,
            ),
          );
        }

        return AnimatedContainer(
          width: isLoading ? 50 : media.size.height,
          height: 50,
          duration: const Duration(milliseconds: 200),
          child: InkWell(
            onTap: () {
              final state = _formKey.currentState;
              if (state != null && state.validate()) {
                context.read(registerStateNotifier.notifier).register(
                      RegisterProfileParams(
                        email: email.value.text,
                        password: pass.value.text,
                        city: city.value.text,
                        name: name.value.text,
                      ),
                    );
              }
            },
            child: Container(
              decoration: BoxDecoration(
                color: theme.colorScheme.primary,
                borderRadius: !isLoading
                    ? BorderRadius.zero
                    : BorderRadius.all(Radius.circular(25)),
                boxShadow: [
                  BoxShadow(
                    color: theme.colorScheme.secondary.withOpacity(0.05),
                    spreadRadius: 0,
                    blurRadius: 10,
                    offset: const Offset(0, 2),
                  ),
                ],
              ),
              child: Center(
                child: AnimatedSwitcher(
                  duration: const Duration(milliseconds: 300),
                  child: isLoading
                      ? CircularLoader(
                          color: theme.colorScheme.surface,
                        )
                      : textWidget,
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildErrorMessage() {
    return Consumer(
      builder: (context, watch, _) {
        final theme = Theme.of(context);
        final state = watch(registerStateNotifier);

        if (state is RegisterError) {
          return Center(
            child: Text(
              state.message,
              style: theme.textTheme.bodyText2!.copyWith(
                color: theme.colorScheme.error,
              ),
            ),
          );
        }
        return EmptyWidget();
      },
    );
  }

  Widget _buildDogImage() {
    return Builder(
      builder: (context) {
        return Container(
          child: Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(math.pi),
            child: Image.asset('assets/images/dog.png'),
          ),
        );
      },
    );
  }
}
