import 'package:find_your_pet/features/presentation/widgets/empty_widget.dart';
import 'package:find_your_pet/features/presentation/widgets/response_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class InputPage extends HookWidget {
  final String buttonTitle;
  final String labelText;
  final Function(String)? onSend;
  final TextInputType? keyboardType;

  const InputPage({
    Key? key,
    required this.buttonTitle,
    required this.labelText,
    this.keyboardType = TextInputType.text,
    this.onSend,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final inputController =
        useTextEditingController.fromValue(TextEditingValue.empty);

    final theme = Theme.of(context);

    return Scaffold(
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Align(
                alignment: Alignment.topLeft,
                child: IconButton(
                  onPressed: () => Navigator.of(context).pop(),
                  icon: Icon(Icons.arrow_back),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15),
              child: TextFormField(
                controller: inputController,
                autofocus: true,
                style: theme.textTheme.bodyText1,
                keyboardType: keyboardType,
                maxLength: 128,
                decoration: InputDecoration(
                  isDense: true,
                  labelStyle: theme.textTheme.bodyText1,
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 5,
                    vertical: 10,
                  ),
                ),
                onFieldSubmitted: (value) => onSend?.call(value),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Text(
                labelText,
                style: theme.textTheme.caption?.copyWith(
                  color: theme.colorScheme.onBackground.withOpacity(0.70),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                    onPressed: () => onSend?.call(inputController.text),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 15,
                      ),
                      child: Text(
                        buttonTitle,
                        style: theme.textTheme.bodyText1?.copyWith(
                          color: theme.colorScheme.onPrimary,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
