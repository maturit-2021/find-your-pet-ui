import 'package:find_your_pet/features/infrastructure/notifiers/auth/login/login_state.dart';
import 'package:find_your_pet/features/infrastructure/notifiers/auth/login/login_state_notifier.dart';
import 'package:find_your_pet/features/presentation/widgets/circular_loader.dart';
import 'package:find_your_pet/features/presentation/widgets/logo.dart';
import 'package:find_your_pet/features/presentation/widgets/response_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:find_your_pet/core/extensions/stringX.dart';

class LoginPage extends HookWidget {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final formContainer = buildFormContainer(context);
    final size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        body: ResponsiveLayout(
          mobile: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  width: size.width / 2,
                  child: _buildCatImage(),
                ),
                formContainer,
              ],
            ),
          ),
          pc: Stack(
            alignment: Alignment.center,
            children: [
              Positioned(
                right: ResponsiveLayout.kHorizontalPadding * 2,
                child: formContainer,
              ),
              Positioned(
                bottom: 0,
                left: 20,
                child: SizedBox(
                  height: 400,
                  child: _buildCatImage(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container buildFormContainer(BuildContext context) {
    final theme = Theme.of(context);
    final size = MediaQuery.of(context).size;
    final isPc = ResponsiveLayout.isPc(context);

    return Container(
      width: isPc ? size.width * 0.35 : null,
      height: isPc ? null : size.height,
      decoration: BoxDecoration(
        color: theme.colorScheme.surface,
        borderRadius: const BorderRadius.all(Radius.circular(4.0)),
        boxShadow: [
          BoxShadow(
            color: theme.colorScheme.onBackground.withOpacity(1),
            spreadRadius: 0,
            blurRadius: 8,
            offset: const Offset(0, 4),
          ),
        ],
      ),
      child: Padding(
        padding: EdgeInsets.all(
          ResponsiveLayout.kHorizontalPadding,
        ),
        child: buildLoginForm(context),
      ),
    );
  }

  Form buildLoginForm(BuildContext context) {
    final theme = Theme.of(context);
    final passwordFocus = FocusNode();
    final emailController =
        useTextEditingController.fromValue(TextEditingValue.empty);
    final passwordController =
        useTextEditingController.fromValue(TextEditingValue.empty);

    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              width: 150,
              child: const Logo(),
            ),
            const SizedBox(
              height: 25,
            ),
            TextFormField(
              controller: emailController,
              autofocus: true,
              style: theme.textTheme.bodyText1,
              keyboardType: TextInputType.emailAddress,
              onEditingComplete: () =>
                  FocusScope.of(context).requestFocus(passwordFocus),
              onFieldSubmitted: (_) => _authenticate(
                context,
                emailController,
                passwordController,
              ),
              decoration: InputDecoration(
                isDense: true,
                labelText: 'Email',
                labelStyle: theme.textTheme.bodyText1,
                contentPadding:
                    const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Required field';
                } else if (!value.isValidEmail()) {
                  return 'Invalid email address';
                }
                return null;
              },
            ),
            const SizedBox(
              height: 30,
            ),
            TextFormField(
              obscureText: true,
              controller: passwordController,
              focusNode: passwordFocus,
              style: theme.textTheme.bodyText1,
              keyboardType: TextInputType.visiblePassword,
              onFieldSubmitted: (_) => _authenticate(
                context,
                emailController,
                passwordController,
              ),
              decoration: InputDecoration(
                isDense: true,
                labelText: 'Password',
                labelStyle: theme.textTheme.bodyText1,
                contentPadding:
                    const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Invalid password';
                }
                return null;
              },
            ),
            const SizedBox(
              height: 15,
            ),
            _buildErrorMessage(),
            const SizedBox(
              height: 15,
            ),
            buildLoginButton(context, emailController, passwordController),
            const SizedBox(
              height: 50,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: InkWell(
                onTap: () => Navigator.pushNamed(context, '/register'),
                child: Text(
                  'Create an account',
                  style: theme.textTheme.bodyText2,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Consumer buildLoginButton(
    BuildContext context,
    TextEditingController emailController,
    TextEditingController passwordController,
  ) {
    final theme = Theme.of(context);
    final media = MediaQuery.of(context);

    final textWidget = Text(
      'Log In',
      style: theme.textTheme.bodyText1?.copyWith(
        color: theme.colorScheme.onError,
        fontWeight: FontWeight.bold,
      ),
    );

    return Consumer(
      builder: (BuildContext context, watch, __) {
        final state = watch(loginStateNotifier);
        final isLoading = state is LoginLoading;

        if (state is LoginLoaded) {
          Future.microtask(
            () => Navigator.pushNamedAndRemoveUntil(
              context,
              '/home',
              (_) => false,
            ),
          );
        }

        return AnimatedContainer(
          width: isLoading ? 50 : media.size.height,
          height: 50,
          duration: const Duration(milliseconds: 100),
          child: InkWell(
            onTap: () => _authenticate(
              context,
              emailController,
              passwordController,
            ),
            child: Container(
              decoration: BoxDecoration(
                color: theme.colorScheme.primary,
                borderRadius: !isLoading
                    ? BorderRadius.zero
                    : BorderRadius.all(Radius.circular(25)),
                boxShadow: [
                  BoxShadow(
                    color: theme.colorScheme.secondary.withOpacity(0.05),
                    spreadRadius: 0,
                    blurRadius: 10,
                    offset: const Offset(0, 2),
                  ),
                ],
              ),
              child: Center(
                child: AnimatedSwitcher(
                  duration: const Duration(milliseconds: 100),
                  child: isLoading
                      ? CircularLoader(
                          color: theme.colorScheme.surface,
                        )
                      : textWidget,
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void _authenticate(
    BuildContext context,
    TextEditingController emailController,
    TextEditingController passwordController,
  ) {
    final state = _formKey.currentState;
    if (state != null && state.validate()) {
      context.read(loginStateNotifier.notifier).authenticate(
            LoginParams(
              email: emailController.value.text,
              password: passwordController.value.text,
            ),
          );
    }
  }

  Widget _buildErrorMessage() {
    return Consumer(
      builder: (context, watch, _) {
        final theme = Theme.of(context);
        final state = watch(loginStateNotifier);

        if (state is LoginError) {
          return Center(
            child: Text(
              state.message,
              style: theme.textTheme.bodyText2!.copyWith(
                color: theme.colorScheme.error,
              ),
            ),
          );
        }
        return Container();
      },
    );
  }

  Widget _buildCatImage() {
    return Builder(
      builder: (context) {
        return Container(
          child: Image.asset('assets/images/cat-login.png'),
        );
      },
    );
  }
}
