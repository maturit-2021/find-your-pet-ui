import 'package:find_your_pet/features/data/logger_repository.dart';
import 'package:find_your_pet/features/presentation/pages/loading/loading_page.dart';
import 'package:find_your_pet/features/presentation/pages/login/login_page.dart';
import 'package:find_your_pet/features/presentation/pages/register/register_page.dart';
import 'package:find_your_pet/features/presentation/widgets/toastr.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'core/utils/theme.dart';
import 'features/presentation/pages/home/home_page.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(
    ProviderScope(
      observers: [
        ProviderContainer().read(loggerChangesInProvider),
      ],
      child: Toastr(
        child: FypApp(),
      ),
    ),
  );
}

class FypApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FYP - Find Your PET',
      theme: lightTheme(),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (_) => LoadingPage(),
        '/home': (_) => HomePage(),
        '/login': (_) => LoginPage(),
        '/register': (_) => RegisterPage(),
      },
    );
  }
}
