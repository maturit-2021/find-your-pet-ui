// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'failures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$FailureTearOff {
  const _$FailureTearOff();

  ServerFailure serverFailure([String? message]) {
    return ServerFailure(
      message,
    );
  }

  UnknowError unkownError([String? message]) {
    return UnknowError(
      message,
    );
  }

  BadResponse badResponse([String? message]) {
    return BadResponse(
      message,
    );
  }

  BadRequest badRequest([String? message]) {
    return BadRequest(
      message,
    );
  }

  UnauthorizedRequest unauthorizedRequest([String? message]) {
    return UnauthorizedRequest(
      message,
    );
  }

  NotFound notFound([String? message]) {
    return NotFound(
      message,
    );
  }

  Unauthenticated unauthenticated([String? message]) {
    return Unauthenticated(
      message,
    );
  }

  GpsNotEnabled gpsNotEnabled([String? message]) {
    return GpsNotEnabled(
      message,
    );
  }

  GpsDenied gpsDenied([String? message]) {
    return GpsDenied(
      message,
    );
  }

  GpsDeniedForever gpsDeniedForever([String? message]) {
    return GpsDeniedForever(
      message,
    );
  }
}

/// @nodoc
const $Failure = _$FailureTearOff();

/// @nodoc
mixin _$Failure {
  String? get message => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) serverFailure,
    required TResult Function(String? message) unkownError,
    required TResult Function(String? message) badResponse,
    required TResult Function(String? message) badRequest,
    required TResult Function(String? message) unauthorizedRequest,
    required TResult Function(String? message) notFound,
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) gpsNotEnabled,
    required TResult Function(String? message) gpsDenied,
    required TResult Function(String? message) gpsDeniedForever,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? serverFailure,
    TResult Function(String? message)? unkownError,
    TResult Function(String? message)? badResponse,
    TResult Function(String? message)? badRequest,
    TResult Function(String? message)? unauthorizedRequest,
    TResult Function(String? message)? notFound,
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? gpsNotEnabled,
    TResult Function(String? message)? gpsDenied,
    TResult Function(String? message)? gpsDeniedForever,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerFailure value) serverFailure,
    required TResult Function(UnknowError value) unkownError,
    required TResult Function(BadResponse value) badResponse,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(UnauthorizedRequest value) unauthorizedRequest,
    required TResult Function(NotFound value) notFound,
    required TResult Function(Unauthenticated value) unauthenticated,
    required TResult Function(GpsNotEnabled value) gpsNotEnabled,
    required TResult Function(GpsDenied value) gpsDenied,
    required TResult Function(GpsDeniedForever value) gpsDeniedForever,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerFailure value)? serverFailure,
    TResult Function(UnknowError value)? unkownError,
    TResult Function(BadResponse value)? badResponse,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(UnauthorizedRequest value)? unauthorizedRequest,
    TResult Function(NotFound value)? notFound,
    TResult Function(Unauthenticated value)? unauthenticated,
    TResult Function(GpsNotEnabled value)? gpsNotEnabled,
    TResult Function(GpsDenied value)? gpsDenied,
    TResult Function(GpsDeniedForever value)? gpsDeniedForever,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FailureCopyWith<Failure> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FailureCopyWith<$Res> {
  factory $FailureCopyWith(Failure value, $Res Function(Failure) then) =
      _$FailureCopyWithImpl<$Res>;
  $Res call({String? message});
}

/// @nodoc
class _$FailureCopyWithImpl<$Res> implements $FailureCopyWith<$Res> {
  _$FailureCopyWithImpl(this._value, this._then);

  final Failure _value;
  // ignore: unused_field
  final $Res Function(Failure) _then;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_value.copyWith(
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class $ServerFailureCopyWith<$Res> implements $FailureCopyWith<$Res> {
  factory $ServerFailureCopyWith(
          ServerFailure value, $Res Function(ServerFailure) then) =
      _$ServerFailureCopyWithImpl<$Res>;
  @override
  $Res call({String? message});
}

/// @nodoc
class _$ServerFailureCopyWithImpl<$Res> extends _$FailureCopyWithImpl<$Res>
    implements $ServerFailureCopyWith<$Res> {
  _$ServerFailureCopyWithImpl(
      ServerFailure _value, $Res Function(ServerFailure) _then)
      : super(_value, (v) => _then(v as ServerFailure));

  @override
  ServerFailure get _value => super._value as ServerFailure;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(ServerFailure(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$ServerFailure implements ServerFailure {
  const _$ServerFailure([this.message]);

  @override
  final String? message;

  @override
  String toString() {
    return 'Failure.serverFailure(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ServerFailure &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  $ServerFailureCopyWith<ServerFailure> get copyWith =>
      _$ServerFailureCopyWithImpl<ServerFailure>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) serverFailure,
    required TResult Function(String? message) unkownError,
    required TResult Function(String? message) badResponse,
    required TResult Function(String? message) badRequest,
    required TResult Function(String? message) unauthorizedRequest,
    required TResult Function(String? message) notFound,
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) gpsNotEnabled,
    required TResult Function(String? message) gpsDenied,
    required TResult Function(String? message) gpsDeniedForever,
  }) {
    return serverFailure(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? serverFailure,
    TResult Function(String? message)? unkownError,
    TResult Function(String? message)? badResponse,
    TResult Function(String? message)? badRequest,
    TResult Function(String? message)? unauthorizedRequest,
    TResult Function(String? message)? notFound,
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? gpsNotEnabled,
    TResult Function(String? message)? gpsDenied,
    TResult Function(String? message)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (serverFailure != null) {
      return serverFailure(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerFailure value) serverFailure,
    required TResult Function(UnknowError value) unkownError,
    required TResult Function(BadResponse value) badResponse,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(UnauthorizedRequest value) unauthorizedRequest,
    required TResult Function(NotFound value) notFound,
    required TResult Function(Unauthenticated value) unauthenticated,
    required TResult Function(GpsNotEnabled value) gpsNotEnabled,
    required TResult Function(GpsDenied value) gpsDenied,
    required TResult Function(GpsDeniedForever value) gpsDeniedForever,
  }) {
    return serverFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerFailure value)? serverFailure,
    TResult Function(UnknowError value)? unkownError,
    TResult Function(BadResponse value)? badResponse,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(UnauthorizedRequest value)? unauthorizedRequest,
    TResult Function(NotFound value)? notFound,
    TResult Function(Unauthenticated value)? unauthenticated,
    TResult Function(GpsNotEnabled value)? gpsNotEnabled,
    TResult Function(GpsDenied value)? gpsDenied,
    TResult Function(GpsDeniedForever value)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (serverFailure != null) {
      return serverFailure(this);
    }
    return orElse();
  }
}

abstract class ServerFailure implements Failure {
  const factory ServerFailure([String? message]) = _$ServerFailure;

  @override
  String? get message => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $ServerFailureCopyWith<ServerFailure> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UnknowErrorCopyWith<$Res> implements $FailureCopyWith<$Res> {
  factory $UnknowErrorCopyWith(
          UnknowError value, $Res Function(UnknowError) then) =
      _$UnknowErrorCopyWithImpl<$Res>;
  @override
  $Res call({String? message});
}

/// @nodoc
class _$UnknowErrorCopyWithImpl<$Res> extends _$FailureCopyWithImpl<$Res>
    implements $UnknowErrorCopyWith<$Res> {
  _$UnknowErrorCopyWithImpl(
      UnknowError _value, $Res Function(UnknowError) _then)
      : super(_value, (v) => _then(v as UnknowError));

  @override
  UnknowError get _value => super._value as UnknowError;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(UnknowError(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$UnknowError implements UnknowError {
  const _$UnknowError([this.message]);

  @override
  final String? message;

  @override
  String toString() {
    return 'Failure.unkownError(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UnknowError &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  $UnknowErrorCopyWith<UnknowError> get copyWith =>
      _$UnknowErrorCopyWithImpl<UnknowError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) serverFailure,
    required TResult Function(String? message) unkownError,
    required TResult Function(String? message) badResponse,
    required TResult Function(String? message) badRequest,
    required TResult Function(String? message) unauthorizedRequest,
    required TResult Function(String? message) notFound,
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) gpsNotEnabled,
    required TResult Function(String? message) gpsDenied,
    required TResult Function(String? message) gpsDeniedForever,
  }) {
    return unkownError(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? serverFailure,
    TResult Function(String? message)? unkownError,
    TResult Function(String? message)? badResponse,
    TResult Function(String? message)? badRequest,
    TResult Function(String? message)? unauthorizedRequest,
    TResult Function(String? message)? notFound,
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? gpsNotEnabled,
    TResult Function(String? message)? gpsDenied,
    TResult Function(String? message)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (unkownError != null) {
      return unkownError(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerFailure value) serverFailure,
    required TResult Function(UnknowError value) unkownError,
    required TResult Function(BadResponse value) badResponse,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(UnauthorizedRequest value) unauthorizedRequest,
    required TResult Function(NotFound value) notFound,
    required TResult Function(Unauthenticated value) unauthenticated,
    required TResult Function(GpsNotEnabled value) gpsNotEnabled,
    required TResult Function(GpsDenied value) gpsDenied,
    required TResult Function(GpsDeniedForever value) gpsDeniedForever,
  }) {
    return unkownError(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerFailure value)? serverFailure,
    TResult Function(UnknowError value)? unkownError,
    TResult Function(BadResponse value)? badResponse,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(UnauthorizedRequest value)? unauthorizedRequest,
    TResult Function(NotFound value)? notFound,
    TResult Function(Unauthenticated value)? unauthenticated,
    TResult Function(GpsNotEnabled value)? gpsNotEnabled,
    TResult Function(GpsDenied value)? gpsDenied,
    TResult Function(GpsDeniedForever value)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (unkownError != null) {
      return unkownError(this);
    }
    return orElse();
  }
}

abstract class UnknowError implements Failure {
  const factory UnknowError([String? message]) = _$UnknowError;

  @override
  String? get message => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $UnknowErrorCopyWith<UnknowError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BadResponseCopyWith<$Res> implements $FailureCopyWith<$Res> {
  factory $BadResponseCopyWith(
          BadResponse value, $Res Function(BadResponse) then) =
      _$BadResponseCopyWithImpl<$Res>;
  @override
  $Res call({String? message});
}

/// @nodoc
class _$BadResponseCopyWithImpl<$Res> extends _$FailureCopyWithImpl<$Res>
    implements $BadResponseCopyWith<$Res> {
  _$BadResponseCopyWithImpl(
      BadResponse _value, $Res Function(BadResponse) _then)
      : super(_value, (v) => _then(v as BadResponse));

  @override
  BadResponse get _value => super._value as BadResponse;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(BadResponse(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$BadResponse implements BadResponse {
  const _$BadResponse([this.message]);

  @override
  final String? message;

  @override
  String toString() {
    return 'Failure.badResponse(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is BadResponse &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  $BadResponseCopyWith<BadResponse> get copyWith =>
      _$BadResponseCopyWithImpl<BadResponse>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) serverFailure,
    required TResult Function(String? message) unkownError,
    required TResult Function(String? message) badResponse,
    required TResult Function(String? message) badRequest,
    required TResult Function(String? message) unauthorizedRequest,
    required TResult Function(String? message) notFound,
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) gpsNotEnabled,
    required TResult Function(String? message) gpsDenied,
    required TResult Function(String? message) gpsDeniedForever,
  }) {
    return badResponse(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? serverFailure,
    TResult Function(String? message)? unkownError,
    TResult Function(String? message)? badResponse,
    TResult Function(String? message)? badRequest,
    TResult Function(String? message)? unauthorizedRequest,
    TResult Function(String? message)? notFound,
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? gpsNotEnabled,
    TResult Function(String? message)? gpsDenied,
    TResult Function(String? message)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (badResponse != null) {
      return badResponse(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerFailure value) serverFailure,
    required TResult Function(UnknowError value) unkownError,
    required TResult Function(BadResponse value) badResponse,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(UnauthorizedRequest value) unauthorizedRequest,
    required TResult Function(NotFound value) notFound,
    required TResult Function(Unauthenticated value) unauthenticated,
    required TResult Function(GpsNotEnabled value) gpsNotEnabled,
    required TResult Function(GpsDenied value) gpsDenied,
    required TResult Function(GpsDeniedForever value) gpsDeniedForever,
  }) {
    return badResponse(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerFailure value)? serverFailure,
    TResult Function(UnknowError value)? unkownError,
    TResult Function(BadResponse value)? badResponse,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(UnauthorizedRequest value)? unauthorizedRequest,
    TResult Function(NotFound value)? notFound,
    TResult Function(Unauthenticated value)? unauthenticated,
    TResult Function(GpsNotEnabled value)? gpsNotEnabled,
    TResult Function(GpsDenied value)? gpsDenied,
    TResult Function(GpsDeniedForever value)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (badResponse != null) {
      return badResponse(this);
    }
    return orElse();
  }
}

abstract class BadResponse implements Failure {
  const factory BadResponse([String? message]) = _$BadResponse;

  @override
  String? get message => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $BadResponseCopyWith<BadResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BadRequestCopyWith<$Res> implements $FailureCopyWith<$Res> {
  factory $BadRequestCopyWith(
          BadRequest value, $Res Function(BadRequest) then) =
      _$BadRequestCopyWithImpl<$Res>;
  @override
  $Res call({String? message});
}

/// @nodoc
class _$BadRequestCopyWithImpl<$Res> extends _$FailureCopyWithImpl<$Res>
    implements $BadRequestCopyWith<$Res> {
  _$BadRequestCopyWithImpl(BadRequest _value, $Res Function(BadRequest) _then)
      : super(_value, (v) => _then(v as BadRequest));

  @override
  BadRequest get _value => super._value as BadRequest;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(BadRequest(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$BadRequest implements BadRequest {
  const _$BadRequest([this.message]);

  @override
  final String? message;

  @override
  String toString() {
    return 'Failure.badRequest(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is BadRequest &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  $BadRequestCopyWith<BadRequest> get copyWith =>
      _$BadRequestCopyWithImpl<BadRequest>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) serverFailure,
    required TResult Function(String? message) unkownError,
    required TResult Function(String? message) badResponse,
    required TResult Function(String? message) badRequest,
    required TResult Function(String? message) unauthorizedRequest,
    required TResult Function(String? message) notFound,
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) gpsNotEnabled,
    required TResult Function(String? message) gpsDenied,
    required TResult Function(String? message) gpsDeniedForever,
  }) {
    return badRequest(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? serverFailure,
    TResult Function(String? message)? unkownError,
    TResult Function(String? message)? badResponse,
    TResult Function(String? message)? badRequest,
    TResult Function(String? message)? unauthorizedRequest,
    TResult Function(String? message)? notFound,
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? gpsNotEnabled,
    TResult Function(String? message)? gpsDenied,
    TResult Function(String? message)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (badRequest != null) {
      return badRequest(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerFailure value) serverFailure,
    required TResult Function(UnknowError value) unkownError,
    required TResult Function(BadResponse value) badResponse,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(UnauthorizedRequest value) unauthorizedRequest,
    required TResult Function(NotFound value) notFound,
    required TResult Function(Unauthenticated value) unauthenticated,
    required TResult Function(GpsNotEnabled value) gpsNotEnabled,
    required TResult Function(GpsDenied value) gpsDenied,
    required TResult Function(GpsDeniedForever value) gpsDeniedForever,
  }) {
    return badRequest(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerFailure value)? serverFailure,
    TResult Function(UnknowError value)? unkownError,
    TResult Function(BadResponse value)? badResponse,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(UnauthorizedRequest value)? unauthorizedRequest,
    TResult Function(NotFound value)? notFound,
    TResult Function(Unauthenticated value)? unauthenticated,
    TResult Function(GpsNotEnabled value)? gpsNotEnabled,
    TResult Function(GpsDenied value)? gpsDenied,
    TResult Function(GpsDeniedForever value)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (badRequest != null) {
      return badRequest(this);
    }
    return orElse();
  }
}

abstract class BadRequest implements Failure {
  const factory BadRequest([String? message]) = _$BadRequest;

  @override
  String? get message => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $BadRequestCopyWith<BadRequest> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UnauthorizedRequestCopyWith<$Res>
    implements $FailureCopyWith<$Res> {
  factory $UnauthorizedRequestCopyWith(
          UnauthorizedRequest value, $Res Function(UnauthorizedRequest) then) =
      _$UnauthorizedRequestCopyWithImpl<$Res>;
  @override
  $Res call({String? message});
}

/// @nodoc
class _$UnauthorizedRequestCopyWithImpl<$Res>
    extends _$FailureCopyWithImpl<$Res>
    implements $UnauthorizedRequestCopyWith<$Res> {
  _$UnauthorizedRequestCopyWithImpl(
      UnauthorizedRequest _value, $Res Function(UnauthorizedRequest) _then)
      : super(_value, (v) => _then(v as UnauthorizedRequest));

  @override
  UnauthorizedRequest get _value => super._value as UnauthorizedRequest;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(UnauthorizedRequest(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$UnauthorizedRequest implements UnauthorizedRequest {
  const _$UnauthorizedRequest([this.message]);

  @override
  final String? message;

  @override
  String toString() {
    return 'Failure.unauthorizedRequest(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UnauthorizedRequest &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  $UnauthorizedRequestCopyWith<UnauthorizedRequest> get copyWith =>
      _$UnauthorizedRequestCopyWithImpl<UnauthorizedRequest>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) serverFailure,
    required TResult Function(String? message) unkownError,
    required TResult Function(String? message) badResponse,
    required TResult Function(String? message) badRequest,
    required TResult Function(String? message) unauthorizedRequest,
    required TResult Function(String? message) notFound,
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) gpsNotEnabled,
    required TResult Function(String? message) gpsDenied,
    required TResult Function(String? message) gpsDeniedForever,
  }) {
    return unauthorizedRequest(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? serverFailure,
    TResult Function(String? message)? unkownError,
    TResult Function(String? message)? badResponse,
    TResult Function(String? message)? badRequest,
    TResult Function(String? message)? unauthorizedRequest,
    TResult Function(String? message)? notFound,
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? gpsNotEnabled,
    TResult Function(String? message)? gpsDenied,
    TResult Function(String? message)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (unauthorizedRequest != null) {
      return unauthorizedRequest(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerFailure value) serverFailure,
    required TResult Function(UnknowError value) unkownError,
    required TResult Function(BadResponse value) badResponse,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(UnauthorizedRequest value) unauthorizedRequest,
    required TResult Function(NotFound value) notFound,
    required TResult Function(Unauthenticated value) unauthenticated,
    required TResult Function(GpsNotEnabled value) gpsNotEnabled,
    required TResult Function(GpsDenied value) gpsDenied,
    required TResult Function(GpsDeniedForever value) gpsDeniedForever,
  }) {
    return unauthorizedRequest(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerFailure value)? serverFailure,
    TResult Function(UnknowError value)? unkownError,
    TResult Function(BadResponse value)? badResponse,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(UnauthorizedRequest value)? unauthorizedRequest,
    TResult Function(NotFound value)? notFound,
    TResult Function(Unauthenticated value)? unauthenticated,
    TResult Function(GpsNotEnabled value)? gpsNotEnabled,
    TResult Function(GpsDenied value)? gpsDenied,
    TResult Function(GpsDeniedForever value)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (unauthorizedRequest != null) {
      return unauthorizedRequest(this);
    }
    return orElse();
  }
}

abstract class UnauthorizedRequest implements Failure {
  const factory UnauthorizedRequest([String? message]) = _$UnauthorizedRequest;

  @override
  String? get message => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $UnauthorizedRequestCopyWith<UnauthorizedRequest> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NotFoundCopyWith<$Res> implements $FailureCopyWith<$Res> {
  factory $NotFoundCopyWith(NotFound value, $Res Function(NotFound) then) =
      _$NotFoundCopyWithImpl<$Res>;
  @override
  $Res call({String? message});
}

/// @nodoc
class _$NotFoundCopyWithImpl<$Res> extends _$FailureCopyWithImpl<$Res>
    implements $NotFoundCopyWith<$Res> {
  _$NotFoundCopyWithImpl(NotFound _value, $Res Function(NotFound) _then)
      : super(_value, (v) => _then(v as NotFound));

  @override
  NotFound get _value => super._value as NotFound;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(NotFound(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$NotFound implements NotFound {
  const _$NotFound([this.message]);

  @override
  final String? message;

  @override
  String toString() {
    return 'Failure.notFound(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is NotFound &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  $NotFoundCopyWith<NotFound> get copyWith =>
      _$NotFoundCopyWithImpl<NotFound>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) serverFailure,
    required TResult Function(String? message) unkownError,
    required TResult Function(String? message) badResponse,
    required TResult Function(String? message) badRequest,
    required TResult Function(String? message) unauthorizedRequest,
    required TResult Function(String? message) notFound,
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) gpsNotEnabled,
    required TResult Function(String? message) gpsDenied,
    required TResult Function(String? message) gpsDeniedForever,
  }) {
    return notFound(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? serverFailure,
    TResult Function(String? message)? unkownError,
    TResult Function(String? message)? badResponse,
    TResult Function(String? message)? badRequest,
    TResult Function(String? message)? unauthorizedRequest,
    TResult Function(String? message)? notFound,
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? gpsNotEnabled,
    TResult Function(String? message)? gpsDenied,
    TResult Function(String? message)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (notFound != null) {
      return notFound(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerFailure value) serverFailure,
    required TResult Function(UnknowError value) unkownError,
    required TResult Function(BadResponse value) badResponse,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(UnauthorizedRequest value) unauthorizedRequest,
    required TResult Function(NotFound value) notFound,
    required TResult Function(Unauthenticated value) unauthenticated,
    required TResult Function(GpsNotEnabled value) gpsNotEnabled,
    required TResult Function(GpsDenied value) gpsDenied,
    required TResult Function(GpsDeniedForever value) gpsDeniedForever,
  }) {
    return notFound(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerFailure value)? serverFailure,
    TResult Function(UnknowError value)? unkownError,
    TResult Function(BadResponse value)? badResponse,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(UnauthorizedRequest value)? unauthorizedRequest,
    TResult Function(NotFound value)? notFound,
    TResult Function(Unauthenticated value)? unauthenticated,
    TResult Function(GpsNotEnabled value)? gpsNotEnabled,
    TResult Function(GpsDenied value)? gpsDenied,
    TResult Function(GpsDeniedForever value)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (notFound != null) {
      return notFound(this);
    }
    return orElse();
  }
}

abstract class NotFound implements Failure {
  const factory NotFound([String? message]) = _$NotFound;

  @override
  String? get message => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $NotFoundCopyWith<NotFound> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UnauthenticatedCopyWith<$Res>
    implements $FailureCopyWith<$Res> {
  factory $UnauthenticatedCopyWith(
          Unauthenticated value, $Res Function(Unauthenticated) then) =
      _$UnauthenticatedCopyWithImpl<$Res>;
  @override
  $Res call({String? message});
}

/// @nodoc
class _$UnauthenticatedCopyWithImpl<$Res> extends _$FailureCopyWithImpl<$Res>
    implements $UnauthenticatedCopyWith<$Res> {
  _$UnauthenticatedCopyWithImpl(
      Unauthenticated _value, $Res Function(Unauthenticated) _then)
      : super(_value, (v) => _then(v as Unauthenticated));

  @override
  Unauthenticated get _value => super._value as Unauthenticated;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(Unauthenticated(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$Unauthenticated implements Unauthenticated {
  const _$Unauthenticated([this.message]);

  @override
  final String? message;

  @override
  String toString() {
    return 'Failure.unauthenticated(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is Unauthenticated &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  $UnauthenticatedCopyWith<Unauthenticated> get copyWith =>
      _$UnauthenticatedCopyWithImpl<Unauthenticated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) serverFailure,
    required TResult Function(String? message) unkownError,
    required TResult Function(String? message) badResponse,
    required TResult Function(String? message) badRequest,
    required TResult Function(String? message) unauthorizedRequest,
    required TResult Function(String? message) notFound,
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) gpsNotEnabled,
    required TResult Function(String? message) gpsDenied,
    required TResult Function(String? message) gpsDeniedForever,
  }) {
    return unauthenticated(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? serverFailure,
    TResult Function(String? message)? unkownError,
    TResult Function(String? message)? badResponse,
    TResult Function(String? message)? badRequest,
    TResult Function(String? message)? unauthorizedRequest,
    TResult Function(String? message)? notFound,
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? gpsNotEnabled,
    TResult Function(String? message)? gpsDenied,
    TResult Function(String? message)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (unauthenticated != null) {
      return unauthenticated(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerFailure value) serverFailure,
    required TResult Function(UnknowError value) unkownError,
    required TResult Function(BadResponse value) badResponse,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(UnauthorizedRequest value) unauthorizedRequest,
    required TResult Function(NotFound value) notFound,
    required TResult Function(Unauthenticated value) unauthenticated,
    required TResult Function(GpsNotEnabled value) gpsNotEnabled,
    required TResult Function(GpsDenied value) gpsDenied,
    required TResult Function(GpsDeniedForever value) gpsDeniedForever,
  }) {
    return unauthenticated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerFailure value)? serverFailure,
    TResult Function(UnknowError value)? unkownError,
    TResult Function(BadResponse value)? badResponse,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(UnauthorizedRequest value)? unauthorizedRequest,
    TResult Function(NotFound value)? notFound,
    TResult Function(Unauthenticated value)? unauthenticated,
    TResult Function(GpsNotEnabled value)? gpsNotEnabled,
    TResult Function(GpsDenied value)? gpsDenied,
    TResult Function(GpsDeniedForever value)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (unauthenticated != null) {
      return unauthenticated(this);
    }
    return orElse();
  }
}

abstract class Unauthenticated implements Failure {
  const factory Unauthenticated([String? message]) = _$Unauthenticated;

  @override
  String? get message => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $UnauthenticatedCopyWith<Unauthenticated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GpsNotEnabledCopyWith<$Res> implements $FailureCopyWith<$Res> {
  factory $GpsNotEnabledCopyWith(
          GpsNotEnabled value, $Res Function(GpsNotEnabled) then) =
      _$GpsNotEnabledCopyWithImpl<$Res>;
  @override
  $Res call({String? message});
}

/// @nodoc
class _$GpsNotEnabledCopyWithImpl<$Res> extends _$FailureCopyWithImpl<$Res>
    implements $GpsNotEnabledCopyWith<$Res> {
  _$GpsNotEnabledCopyWithImpl(
      GpsNotEnabled _value, $Res Function(GpsNotEnabled) _then)
      : super(_value, (v) => _then(v as GpsNotEnabled));

  @override
  GpsNotEnabled get _value => super._value as GpsNotEnabled;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(GpsNotEnabled(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$GpsNotEnabled implements GpsNotEnabled {
  const _$GpsNotEnabled([this.message]);

  @override
  final String? message;

  @override
  String toString() {
    return 'Failure.gpsNotEnabled(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is GpsNotEnabled &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  $GpsNotEnabledCopyWith<GpsNotEnabled> get copyWith =>
      _$GpsNotEnabledCopyWithImpl<GpsNotEnabled>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) serverFailure,
    required TResult Function(String? message) unkownError,
    required TResult Function(String? message) badResponse,
    required TResult Function(String? message) badRequest,
    required TResult Function(String? message) unauthorizedRequest,
    required TResult Function(String? message) notFound,
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) gpsNotEnabled,
    required TResult Function(String? message) gpsDenied,
    required TResult Function(String? message) gpsDeniedForever,
  }) {
    return gpsNotEnabled(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? serverFailure,
    TResult Function(String? message)? unkownError,
    TResult Function(String? message)? badResponse,
    TResult Function(String? message)? badRequest,
    TResult Function(String? message)? unauthorizedRequest,
    TResult Function(String? message)? notFound,
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? gpsNotEnabled,
    TResult Function(String? message)? gpsDenied,
    TResult Function(String? message)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (gpsNotEnabled != null) {
      return gpsNotEnabled(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerFailure value) serverFailure,
    required TResult Function(UnknowError value) unkownError,
    required TResult Function(BadResponse value) badResponse,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(UnauthorizedRequest value) unauthorizedRequest,
    required TResult Function(NotFound value) notFound,
    required TResult Function(Unauthenticated value) unauthenticated,
    required TResult Function(GpsNotEnabled value) gpsNotEnabled,
    required TResult Function(GpsDenied value) gpsDenied,
    required TResult Function(GpsDeniedForever value) gpsDeniedForever,
  }) {
    return gpsNotEnabled(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerFailure value)? serverFailure,
    TResult Function(UnknowError value)? unkownError,
    TResult Function(BadResponse value)? badResponse,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(UnauthorizedRequest value)? unauthorizedRequest,
    TResult Function(NotFound value)? notFound,
    TResult Function(Unauthenticated value)? unauthenticated,
    TResult Function(GpsNotEnabled value)? gpsNotEnabled,
    TResult Function(GpsDenied value)? gpsDenied,
    TResult Function(GpsDeniedForever value)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (gpsNotEnabled != null) {
      return gpsNotEnabled(this);
    }
    return orElse();
  }
}

abstract class GpsNotEnabled implements Failure {
  const factory GpsNotEnabled([String? message]) = _$GpsNotEnabled;

  @override
  String? get message => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $GpsNotEnabledCopyWith<GpsNotEnabled> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GpsDeniedCopyWith<$Res> implements $FailureCopyWith<$Res> {
  factory $GpsDeniedCopyWith(GpsDenied value, $Res Function(GpsDenied) then) =
      _$GpsDeniedCopyWithImpl<$Res>;
  @override
  $Res call({String? message});
}

/// @nodoc
class _$GpsDeniedCopyWithImpl<$Res> extends _$FailureCopyWithImpl<$Res>
    implements $GpsDeniedCopyWith<$Res> {
  _$GpsDeniedCopyWithImpl(GpsDenied _value, $Res Function(GpsDenied) _then)
      : super(_value, (v) => _then(v as GpsDenied));

  @override
  GpsDenied get _value => super._value as GpsDenied;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(GpsDenied(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$GpsDenied implements GpsDenied {
  const _$GpsDenied([this.message]);

  @override
  final String? message;

  @override
  String toString() {
    return 'Failure.gpsDenied(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is GpsDenied &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  $GpsDeniedCopyWith<GpsDenied> get copyWith =>
      _$GpsDeniedCopyWithImpl<GpsDenied>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) serverFailure,
    required TResult Function(String? message) unkownError,
    required TResult Function(String? message) badResponse,
    required TResult Function(String? message) badRequest,
    required TResult Function(String? message) unauthorizedRequest,
    required TResult Function(String? message) notFound,
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) gpsNotEnabled,
    required TResult Function(String? message) gpsDenied,
    required TResult Function(String? message) gpsDeniedForever,
  }) {
    return gpsDenied(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? serverFailure,
    TResult Function(String? message)? unkownError,
    TResult Function(String? message)? badResponse,
    TResult Function(String? message)? badRequest,
    TResult Function(String? message)? unauthorizedRequest,
    TResult Function(String? message)? notFound,
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? gpsNotEnabled,
    TResult Function(String? message)? gpsDenied,
    TResult Function(String? message)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (gpsDenied != null) {
      return gpsDenied(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerFailure value) serverFailure,
    required TResult Function(UnknowError value) unkownError,
    required TResult Function(BadResponse value) badResponse,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(UnauthorizedRequest value) unauthorizedRequest,
    required TResult Function(NotFound value) notFound,
    required TResult Function(Unauthenticated value) unauthenticated,
    required TResult Function(GpsNotEnabled value) gpsNotEnabled,
    required TResult Function(GpsDenied value) gpsDenied,
    required TResult Function(GpsDeniedForever value) gpsDeniedForever,
  }) {
    return gpsDenied(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerFailure value)? serverFailure,
    TResult Function(UnknowError value)? unkownError,
    TResult Function(BadResponse value)? badResponse,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(UnauthorizedRequest value)? unauthorizedRequest,
    TResult Function(NotFound value)? notFound,
    TResult Function(Unauthenticated value)? unauthenticated,
    TResult Function(GpsNotEnabled value)? gpsNotEnabled,
    TResult Function(GpsDenied value)? gpsDenied,
    TResult Function(GpsDeniedForever value)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (gpsDenied != null) {
      return gpsDenied(this);
    }
    return orElse();
  }
}

abstract class GpsDenied implements Failure {
  const factory GpsDenied([String? message]) = _$GpsDenied;

  @override
  String? get message => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $GpsDeniedCopyWith<GpsDenied> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GpsDeniedForeverCopyWith<$Res>
    implements $FailureCopyWith<$Res> {
  factory $GpsDeniedForeverCopyWith(
          GpsDeniedForever value, $Res Function(GpsDeniedForever) then) =
      _$GpsDeniedForeverCopyWithImpl<$Res>;
  @override
  $Res call({String? message});
}

/// @nodoc
class _$GpsDeniedForeverCopyWithImpl<$Res> extends _$FailureCopyWithImpl<$Res>
    implements $GpsDeniedForeverCopyWith<$Res> {
  _$GpsDeniedForeverCopyWithImpl(
      GpsDeniedForever _value, $Res Function(GpsDeniedForever) _then)
      : super(_value, (v) => _then(v as GpsDeniedForever));

  @override
  GpsDeniedForever get _value => super._value as GpsDeniedForever;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(GpsDeniedForever(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$GpsDeniedForever implements GpsDeniedForever {
  const _$GpsDeniedForever([this.message]);

  @override
  final String? message;

  @override
  String toString() {
    return 'Failure.gpsDeniedForever(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is GpsDeniedForever &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  $GpsDeniedForeverCopyWith<GpsDeniedForever> get copyWith =>
      _$GpsDeniedForeverCopyWithImpl<GpsDeniedForever>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) serverFailure,
    required TResult Function(String? message) unkownError,
    required TResult Function(String? message) badResponse,
    required TResult Function(String? message) badRequest,
    required TResult Function(String? message) unauthorizedRequest,
    required TResult Function(String? message) notFound,
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) gpsNotEnabled,
    required TResult Function(String? message) gpsDenied,
    required TResult Function(String? message) gpsDeniedForever,
  }) {
    return gpsDeniedForever(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? serverFailure,
    TResult Function(String? message)? unkownError,
    TResult Function(String? message)? badResponse,
    TResult Function(String? message)? badRequest,
    TResult Function(String? message)? unauthorizedRequest,
    TResult Function(String? message)? notFound,
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? gpsNotEnabled,
    TResult Function(String? message)? gpsDenied,
    TResult Function(String? message)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (gpsDeniedForever != null) {
      return gpsDeniedForever(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerFailure value) serverFailure,
    required TResult Function(UnknowError value) unkownError,
    required TResult Function(BadResponse value) badResponse,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(UnauthorizedRequest value) unauthorizedRequest,
    required TResult Function(NotFound value) notFound,
    required TResult Function(Unauthenticated value) unauthenticated,
    required TResult Function(GpsNotEnabled value) gpsNotEnabled,
    required TResult Function(GpsDenied value) gpsDenied,
    required TResult Function(GpsDeniedForever value) gpsDeniedForever,
  }) {
    return gpsDeniedForever(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerFailure value)? serverFailure,
    TResult Function(UnknowError value)? unkownError,
    TResult Function(BadResponse value)? badResponse,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(UnauthorizedRequest value)? unauthorizedRequest,
    TResult Function(NotFound value)? notFound,
    TResult Function(Unauthenticated value)? unauthenticated,
    TResult Function(GpsNotEnabled value)? gpsNotEnabled,
    TResult Function(GpsDenied value)? gpsDenied,
    TResult Function(GpsDeniedForever value)? gpsDeniedForever,
    required TResult orElse(),
  }) {
    if (gpsDeniedForever != null) {
      return gpsDeniedForever(this);
    }
    return orElse();
  }
}

abstract class GpsDeniedForever implements Failure {
  const factory GpsDeniedForever([String? message]) = _$GpsDeniedForever;

  @override
  String? get message => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $GpsDeniedForeverCopyWith<GpsDeniedForever> get copyWith =>
      throw _privateConstructorUsedError;
}
