import 'package:freezed_annotation/freezed_annotation.dart';

part 'failures.freezed.dart';

@freezed
class Failure with _$Failure {
  const factory Failure.serverFailure([String? message]) = ServerFailure;
  const factory Failure.unkownError([String? message]) = UnknowError;

  const factory Failure.badResponse([String? message]) = BadResponse;

  const factory Failure.badRequest([String? message]) = BadRequest;
  const factory Failure.unauthorizedRequest([String? message]) =
      UnauthorizedRequest;
  const factory Failure.notFound([String? message]) = NotFound;

  const factory Failure.unauthenticated([String? message]) = Unauthenticated;

  const factory Failure.gpsNotEnabled([String? message]) = GpsNotEnabled;
  const factory Failure.gpsDenied([String? message]) = GpsDenied;
  const factory Failure.gpsDeniedForever([String? message]) = GpsDeniedForever;
}
