import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

const Color _primaryColor = const Color(0xFFf8a488);
const Color _primaryVariant = const Color(0xFFf7b6a1);

const Color _accColor = const Color(0xFF5aa897);
const Color _accVariant = const Color(0xFF6eccb8);

const Color _darkColor = const Color(0xFF45526c);
const Color _lightColor = const Color(0xFFFFFFFF);

ThemeData lightTheme() {
  const Color onBackground = _darkColor;

  const Color surface = _lightColor;
  const Color onSurface = _darkColor;

  const Color error = const Color(0xFFb8593b);
  const Color onError = _lightColor;

  /// Status bar color and system bottom navigation
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      systemNavigationBarColor: _primaryColor,
      statusBarColor: _primaryColor,
    ),
  );

  return ThemeData.dark().copyWith(
    scaffoldBackgroundColor: _lightColor,
    brightness: Brightness.light,
    backgroundColor: _lightColor,
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      foregroundColor: _lightColor,
      backgroundColor: _primaryColor,
    ),
    colorScheme: ColorScheme.light(
      primary: _primaryColor,
      primaryVariant: _primaryVariant,
      secondary: _accColor,
      secondaryVariant: _accVariant,
      background: _darkColor,
      onBackground: onBackground,
      surface: surface,
      onSurface: onSurface,
      error: error,
      onError: onError,
    ),
    appBarTheme: AppBarTheme(
      backgroundColor: _darkColor.withOpacity(0),
      centerTitle: true,
      elevation: 0,
    ),
    cardTheme: CardTheme(
      elevation: 4,
      color: surface,
      shadowColor: _darkColor.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
    ),
    iconTheme: IconThemeData(
      color: _darkColor.withOpacity(0.6),
      size: 40,
    ),
    dialogTheme: DialogTheme(
      backgroundColor: surface,
      titleTextStyle: const TextStyle(
        fontFamily: 'Roboto',
        fontSize: 18,
        color: _darkColor,
        fontWeight: FontWeight.w600,
      ),
      contentTextStyle: const TextStyle(
        fontFamily: 'Roboto',
        fontSize: 16,
        color: _darkColor,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
    ),
    popupMenuTheme: PopupMenuThemeData(
      color: _lightColor,
      enableFeedback: true,
      textStyle: TextStyle(
        fontFamily: 'Roboto',
        fontSize: 16,
        color: _darkColor,
      ),
    ),
    textTheme: TextTheme(
      caption: TextStyle(
        fontFamily: 'Roboto',
        fontSize: 14,
        color: _darkColor,
      ),
      subtitle1: TextStyle(
        fontFamily: 'Roboto',
        fontSize: 14,
        color: _darkColor.withOpacity(0.6),
      ),
      bodyText1: TextStyle(
        fontFamily: 'Roboto',
        fontSize: 18,
        color: _darkColor,
      ),
      bodyText2: TextStyle(
        fontFamily: 'Roboto',
        fontSize: 16,
        color: _darkColor,
      ),
      headline1: TextStyle(
        fontFamily: 'Roboto',
        fontSize: 40,
        color: _darkColor,
      ),
      headline2: TextStyle(
        fontFamily: 'Roboto',
        fontSize: 36,
        color: _darkColor,
      ),
      headline3: TextStyle(
        fontFamily: 'Roboto',
        fontSize: 32,
        color: _darkColor,
      ),
      headline4: TextStyle(
        fontFamily: 'Roboto',
        fontSize: 28,
        color: _darkColor,
      ),
      headline5: TextStyle(
        fontFamily: 'Roboto',
        fontSize: 24,
        color: _darkColor,
      ),
    ),
  );
}
