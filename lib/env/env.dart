import 'package:flutter/foundation.dart';

abstract class Env {
  static Env? _instance;

  String get baseUrl;
  String get wsBaseUrl;

  static Env get instance {
    if (_instance == null) _instance = kReleaseMode ? _ProdEnv() : _DevEnv();

    return _instance!;
  }
}

class _ProdEnv extends Env {
  String get baseUrl => 'https://fyp.leoletto.me/api';
  String get wsBaseUrl => 'https://fyp.leoletto.me/api';
}

class _DevEnv extends Env {
  // String get baseUrl => 'http://192.168.1.171:3000';
  // String get wsBaseUrl => 'http://192.168.1.171:3000';

  String get baseUrl => 'http://localhost:3000';
  String get wsBaseUrl => 'http://localhost:3000';
}
